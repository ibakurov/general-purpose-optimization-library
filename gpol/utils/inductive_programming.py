import os
import time
import errno
import pickle
import random

import re
from sympy import symbols, simplify
from sympy.utilities.lambdify import lambdastr

import pandas as pd

import torch
import torch.nn as nn


# +++++++++++++++++++++++++++ GP's constants set
def get_constant_set(start, end, size, device="cpu"):
    step, i = (end - start) / size, 0
    constant_set = []
    for _ in range(size):
        constant_set.append(torch.tensor([start + i], device=device))
        i += step
    return constant_set


# +++++++++++++++++++++++++++ GP's functions set
class _Function(object):
    """Implements a mathematical operator.

    This object is called with PyTorch tensor(s) and returns
    a vector resulting form the underling a mathematical operator.
    This class was inspired by gplearn's implementation [1].

    Attributes
    ----------
    function_ : callable
        A function with signature function(x1, *args) that returns a Numpy
        array of the same shape as its arguments.
    name : str
        The name for the function as it should be represented in the program
        and its visualizations.
    arity : int
        The number of arguments that the ``function`` takes.

    References
    ----------
    .. [1] T. Stephens, github.com/trevorstephens/gplearn.
    """
    def __init__(self, function_, name, arity):
        self.function_ = function_
        self.name = name
        self.arity = arity

    def __call__(self, *args):
        return self.function_(*args)

    def __str__(self):
        return self.name

    def __repr__(self):
        return self.__str__()


def make_function(function_, name, arity):
    """Creates an instance of type _Function.

    Creates a mathematical operator as in [1].

    References
    ----------
    .. [1] T. Stephens, github.com/trevorstephens/gplearn.

    Parameters
    ----------
    function_ : callable
        A function with signature `function(x1, *args)` that returns a
        torch.Tensor of the same shape as its arguments.
    name : str
        Function's name as it should be represented in the solution's
        representation.
    arity : int
        The number of arguments the function takes.

    Returns
    -------
    _Function
        An instance of type _Function.
    """
    return _Function(function_, name, arity)


# +++++++++++++++++++++++++++ Protected functions
def protected_div(x1, x2):
    """Implements the so-called Koza division.

    In traditional arithmetic, division by zero is undefined.
    To address this in GP, where division is commonly used along
    with other arithmetic to evolve programs, Koza introduced the
    so-called protected division operation. This approach ensures
    that the division returns a predefined value if the denominator
    is zero. Often, it is 1 or the numerator itself.

    This function returns the value(s) 1 if x2 is (or has) zero(s).

    Parameters
    ----------
    x1 : torch.Tensor
        The numerator.
    x2 : torch.Tensor
        The denominator.

    Returns
    -------
    torch.Tensor
        Result of protected division between x1 and x2.
    """
    return torch.where(torch.abs(x2) > 0.001, torch.div(x1, x2), torch.tensor(1.0, dtype=x2.dtype, device=x2.device))


def protected_log(x):
    """Implements the logarithm protected against non-positives.

    Applies the natural logarithm function of on the elements of the
    input tensor. When the value(s) are zero or below, returns the
    input's values.

    Parameters
    ----------
    x : torch.Tensor
        The input tensor.

    Returns
    -------
    torch.Tensor
        Returns a new tensor with the natural logarithm of the
        elements of the input.
    """
    return torch.where(x > 0.001, torch.log(x), torch.tensor(1., dtype=x.dtype, device=x.device))


def _protected_stack(x1, x2):
    """ Performs a protected stacking of two tensors

     The original torch.stack function cannot stack different shaped
     tensors, which is frequently necessary when stacking some tensor
     with a constant (also a tensor) during min/max/mean operations
     involving two operands. This function performs an appropriate
     re-shaping.

    Parameters
    ----------
    x1 : torch.Tensor
        First operand.
    x2 : torch.Tensor
        Second operand.

    Returns
    -------
    torch.Tensor
        Stacked tensors.
    """
    if x1.shape == x2.shape:
        return torch.stack([x1, x2])
    else:
        t_b = x1 if x1.shape > x2.shape else x2
        t_s = x1 if x1.shape < x2.shape else x2
        return torch.stack([t_b, t_s.repeat(*t_b.shape)])


def protected_min(t1, t2):
    """ Returns the minimum between two tensors at each index

    To perform the min operation between the values of the two tensors
    at the same index.

    Returns
    -------
    torch.Tensor
        Tensor of the same shape as t1 or t2, containing the smallest
        value between the two.
    """
    return torch.min(_protected_stack(t1, t2), dim=0)[0]


def protected_max(t1, t2):
    """ Returns the maximum between two tensors at each index

    To perform the max operation between the values of the two tensors
    at the same index.

    Returns
    -------
    torch.Tensor
        Tensor of the same shape as t1 or t2, containing the largest
        value between the two.
    """
    return torch.max(_protected_stack(t1, t2), dim=0)[0]


def protected_mean(t1, t2):
    """ Returns the average between two tensors at each index

    To perform the max operation between the values of the two tensors
    at the same index.

    Returns
    -------
    torch.Tensor
        Tensor of the same shape as t1 or t2, containing the average
        value between the two.
    """
    return torch.mean(_protected_stack(t1, t2), dim=0)


# +++++++++++++++++++++++++++ Creates the functions' set
# Traditional operators
add2 = _Function(function_=torch.add, name='add', arity=2)
sub2 = _Function(function_=torch.sub, name='sub', arity=2)
mul2 = _Function(function_=torch.mul, name='mul', arity=2)
div2 = _Function(function_=protected_div, name='div', arity=2)
# Other operators
mean2 = _Function(function_=protected_mean, name='mean', arity=2)
max2 = _Function(function_=protected_max, name='max', arity=2)
min2 = _Function(function_=protected_min, name='min', arity=2)
# Non-linearities
log1 = _Function(function_=protected_log, name='log', arity=1)
sin1 = _Function(function_=torch.sin, name='sin', arity=1)
cos1 = _Function(function_=torch.cos, name='cos', arity=1)
# Activation functions
lf1 = _Function(function_=nn.Sigmoid(), name='lf', arity=1)
tanh1 = _Function(function_=nn.Tanh(), name='tanh', arity=1)

# Puts everything into a dictionary
function_map = {'add': add2, 'sub': sub2, 'mul': mul2, 'div': div2, "log": log1, "lf": lf1, "tanh": tanh1,
                "sin": sin1, "cos": cos1, "mean": mean2, "max": max2, "min": min2}


# +++++++++++++++++++++++++++ Trees execution
def get_subtree(repr_):
    """Gets a random subtree from the program.

    This function was inspired by gplearn's implementation [1].

    Parameters
    ----------
    repr_ : list
        Representation of a GP tree as a list of program elements in
        prefix notation.

    Returns
    -------
    start, end : tuple of two ints
        The indices of the start and end of the random subtree.

    References
    ----------
    .. [1] T. Stephens, github.com/trevorstephens/gplearn.
    """
    # Check tree's length: if too small, return the full tree
    if len(repr_) <= 3:
        start, end = 0, len(repr_)
    else:
        probs = torch.tensor([0.9 if isinstance(node, _Function) else 0.1 for node in repr_])
        probs = torch.cumsum(torch.div(probs, probs.sum()), dim=0)
        rnd = random.uniform(0.00001, 0.99999)
        start = (probs >= rnd).nonzero()[0][0].item()
        stack = 1
        end = start
        while stack > end - start:
            node = repr_[end]
            if isinstance(node, _Function):
                stack += node.arity
            end += 1

    return start, end


def _get_tree_depth(repr_):
    """Gets the maximum depth of the program tree.

    This function was inspired by gplearn's implementation [1].

    Parameters
    ----------
    repr_ : list
        Representation of a GP tree as a list of program elements in
        prefix notation.

    Returns
    -------
    depth : int
        Depth of the program tree.

    References
    ----------
    .. [1] T. Stephens, github.com/trevorstephens/gplearn.
    """
    terminals = [0]
    depth = 1
    for node in repr_:
        if isinstance(node, _Function):
            terminals.append(node.arity)
            depth = max(len(terminals), depth)
        else:
            terminals[-1] -= 1
            while terminals[-1] == 0:
                terminals.pop()
                terminals[-1] -= 1
    return depth - 1


def _execute_tree(repr_, X):
    """Executes tree representation on X.

    Processes the input data tensor X using tree representation.
    This function was inspired by gplearn's implementation [1].

    Parameters
    ----------
    repr_ : list
        Representation of a GP tree as a list of program elements in
        prefix notation.
    X : torch.Tensor
        Input data.

    Returns
    -------
    intermediate_result : torch.Tensor
        The output vector of size len(X) generated by processing X using
         the tree representation.

    References
    ----------
    .. [1] T. Stephens, github.com/trevorstephens/gplearn.
    """
    node = repr_[0]
    # Secure against constants' tree
    if isinstance(node, torch.Tensor):
        return node.repeat_interleave(len(X))  # return node
    if isinstance(node, int):
        return X[:, node]

    apply_stack = []

    for node in repr_:
        if isinstance(node, _Function):
            apply_stack.append([node])
        else:
            apply_stack[-1].append(node)

        while len(apply_stack[-1]) == apply_stack[-1][0].arity + 1:
            function_ = apply_stack[-1][0]
            terminals = [X[:, t] if isinstance(t, int) else t for t in apply_stack[-1][1:]]
            intermediate_result = function_(*terminals)
            if len(apply_stack) != 1:
                apply_stack.pop()
                apply_stack[-1].append(intermediate_result)
            else:
                # Secure against constants' tree
                if len(intermediate_result.shape) == 0:
                    return torch.cat(X.shape[0]*[intermediate_result[None]])
                else:
                    return intermediate_result
    return None


# +++++++++++++++++++++++++++ Trees algebraic simplification
def simplify_(repr_, n_features, operators_map):
    """Performs algebraic simplification of a GP tree representation.

    This function essentially integrates SymPy in GPOL. Given the fact
    that SymPy operates upon functions in infix notation, usually
    provided as strings, a series of conversions is has to be
    performed:
        1) Convert repr_ to string representation which symbols are
            readable by SymPy.
        2) Rewrite the formatted string representation using infix
            notation.
        3) Use Sympy to simplify the representation
        4) Rewrite the simplified string representation using prefix
            notation.
        5) Create a list of program elements readable by GPOL from
            the aforementioned string.

    Parameters
    ----------
    repr_ : list
        Representation of a GP tree as a list of program elements in
        prefix notation.
    n_features : int
        Total number of unique input features of the underlying problem.
    operators_map : dict
        Dictionary that maps operators to the SymPy string representation.

    Returns
    -------
    prefix_expression_ : list
        Simplified representation of a GP tree as a list of program
        elements in prefix notation.
    """
    # Cast repr_ to list of program elements as strings
    repr_str = str(repr_)
    repr_str = re.sub("tensor|\(|\)", "", repr_str[1:-1])
    repr_lst_str = repr_str.split(", ")
    # Replace mathematical operator names with symbols (i.e., {"add": "+", "sub": "-", "mul": "*", "div": "/"})
    operators_map_ = {str(k): v for k, v in operators_map.items()}
    repr_lst_str = [operators_map_[item] if item in operators_map_ else item for item in repr_lst_str]
    # Replace integer values with Xn
    operators_list = list(operators_map.values())
    repr_lst_str = [item if item in operators_list or "." in item else "X" + item for item in repr_lst_str]
    # Cast to string
    repr_str_prefix = " ".join(repr_lst_str)
    # Generate symbols for a given number of features
    symbols_ = ["X" + str(i) for i in range(n_features)]
    symbols_ = " ".join(symbols_)
    symbols_ = symbols(symbols_)
    # Cast prefix representation to infix
    repr_str_infix = _prefix_to_infix(repr_str_prefix)
    # Simplifies infix representation
    simple_str = simplify(repr_str_infix)
    # Get string representation
    simple_str = lambdastr(symbols_, simple_str)
    # Preprocess the output string
    simple_str = str(simple_str[simple_str.index("(") + 1: -1].replace(" ", ""))
    # Converts it into a list of program elements in prefix notation.
    prefix_expression = _infix_to_prefix(simple_str)
    # Cast back to GPOL representation
    prefix_expression_ = re.split(" ", prefix_expression.replace("X", ""))
    # Map operators, features and constants
    om_inv = {v: k for k, v in operators_map.items()}
    prefix_expression_ = [om_inv[i] if i in om_inv else (torch.tensor(float(i)) if "." in i else int(i)) for i in
                          prefix_expression_]
    return prefix_expression_


def _prefix_to_infix(prefix):
    """Converts expressions from prefix to infix notation.

    Parameters
    ----------
    prefix : str
        String representation of the mathematical expression in prefix
        notation.

    Returns
    -------
    str
        String representation in infix notation.
    """
    stack = []
    # Split the expression into tokens and then reverse the order of tokens
    tokens = prefix.split()
    tokens.reverse()
    operators = set(['+', '-', '*', '/', '^'])
    # Iterate over the expression for conversion
    for token in tokens:
        if token not in operators:
            # Push operand to stack
            stack.append(token)
        else:
            # Pop two operands
            operand1 = stack.pop()
            operand2 = stack.pop()
            # Combine them with the current operator in infix format
            stack.append(f'({operand1} {token} {operand2})')
    # The final element of the stack is the infix expression
    return stack.pop()


def _infix_to_prefix(infix):
    """Converts expressions from infix to prefix notation.

    Parameters
    ----------
    infix : str
        String representation of the mathematical expression in prefix
        notation.

    Returns
    -------
    str
        String representation in infix notation.
    """
    # Tokenize the expression
    def tokenize(expr):
        # Include support for multi-character operands and decimal numbers
        tokens = re.findall(r"([A-Za-z0-9.]+|\*|\/|\+|\-|\(|\))", expr)
        return tokens

    # Define operator precedence
    precedence = {'+': 1, '-': 1, '*': 2, '/': 2, '^': 3}

    def is_operator(c):
        return c in "+-*/^"

    def precedence_of(c):
        return precedence.get(c, 0)

    # Reverse the order of tokens; handle operands and operators appropriately
    tokens = tokenize(infix)[::-1]

    # Correctly reverse operands (variables and numbers)
    for i in range(len(tokens)):
        if tokens[i] == '(':
            tokens[i] = ')'
        elif tokens[i] == ')':
            tokens[i] = '('

    stack = []  # to keep operators
    postfix = []  # to build the new expression

    for token in tokens:
        if token.isalnum() or '.' in token:  # operand
            postfix.append(token)
        elif token == '(':
            stack.append(token)
        elif token == ')':
            top_token = stack.pop()
            while top_token != '(':
                postfix.append(top_token)
                top_token = stack.pop()
        else:  # operator
            while (stack and precedence_of(stack[-1]) >= precedence_of(token)):
                postfix.append(stack.pop())
            stack.append(token)

    # Pop the remaining operators from the stack
    while stack:
        postfix.append(stack.pop())

    # Convert the postfix list to a prefix string
    prefix = postfix[::-1]

    return ' '.join(prefix)


# +++++++++++++++++++++++++++ Trees semantic simplification
def get_similarity_matrix_hard(semantics, terminals, sim_f=torch.nn.functional.mse_loss):
    """Builds exact similarity matrix based on semantics.

    In practical terms, this function creates a pandas.DataFrame with
    semantically equivalent pairs of subtrees that will be used for the
    replacement algorithm. Each row stores a pair {T(s_i), T(s_j)},
    such that T(s_i) is larger than T(s_j), but both are semantically
    equivalent.

    This function is dedicated for exact simplification. To accommodate for
    the floating-point rounding error, two semantics vectors are considered
    equal if the similarity between them, after being rounded to five
    decimals, equals zero.

    TODO: Join get_similarity_matrix_hard and get_similarity_matrix_soft
        into a single function. Later, this function will be replaced with
        get_similarity_matrix.

    Parameters
    ----------
    semantics : dict
        Dictionary that stores the semantics of each subtree. The
        terminal nodes are also regarded as individual subtrees.
        Each key represents an index of each subtree. Each value
        stores the respective children nodes one depth-level down
        and a torch.Tensor containing the semantics of the subtree.
    terminals : list
        List of integer indexes representing all the terminals nodes
         in a tree.
    sim_f : function
        A function of two inputs (x, y) that is used to compute the
        similarity between the semantics of two subtrees. The output
        of this function determines the semantic equivalency.

    Returns
    -------
    df_sim : pandas.DataFrame
        Data frame of similarity pairs.
    """
    sim_matrix, count_i = [], 0
    semantics_lst = list(semantics.keys())
    for x_i in range(0, len(semantics_lst), 1):
        # Get length of the nodes in a subtree, rooted at node x_i
        nodes_x_i = []
        get_subtree_indices_(x_i, semantics, terminals, nodes_x_i)
        len_x_i = len(nodes_x_i)

        # Avoid computing similarity when Si is a terminal, because there is no need to replace a terminal (size=1)
        if len_x_i == 1:
            continue

        for x_j in range(x_i + 1, len(semantics_lst), 1):
            # Get semantics' shape
            si = semantics[x_i][-1].shape[0] if len(semantics[x_i][-1].shape) > 0 else 1
            sj = semantics[x_j][-1].shape[0] if len(semantics[x_j][-1].shape) > 0 else 1

            # Compute similarity
            sim_i_j = sim_f(semantics[x_i][-1].repeat(sj) if si < sj else semantics[x_i][-1],
                            semantics[x_j][-1].repeat(si) if sj < si else semantics[x_j][-1]).item()

            # Rounds values close to zero:
            if round(sim_i_j, 5) == 0:  # sim_i_j = 0.000009 => 0
                nodes_x_j = []
                get_subtree_indices_(x_j, semantics, terminals, nodes_x_j)
                len_x_j = len(nodes_x_j)
                # Skip iteration for equally sized subtrees
                if len_x_i == len_x_j:
                    continue

                # Populate the list with the largest subtree first
                ordered_list = []
                x_i_smaller = len_x_i < len_x_j
                for i, j in [(x_i, x_j), (len(nodes_x_i), len(nodes_x_j)), (nodes_x_i, nodes_x_j)]:
                    ordered_list.extend([j, i] if x_i_smaller else [i, j])

                # Append similarity
                ordered_list.insert(2, sim_i_j)
                # Append flag stating whether Si C Si
                ordered_list.insert(3, set(nodes_x_i).issubset(set(nodes_x_j)) if x_i_smaller else set(nodes_x_j).issubset(set(nodes_x_i)))
                # Append length of semantics
                ordered_list.insert(4, si)
                ordered_list.insert(5, sj)
                # Append to the distance matrix
                sim_matrix.append(ordered_list)

    df_sim = pd.DataFrame(sim_matrix, columns=["Si", "Sj", "f(Si, Sj)", "Sj C Si", "len(Si)", "len(Sj)",
                                               "len(T(Si))", "len(T(Sj))", "T(Si)", "T(Sj)"])
    return df_sim


def get_similarity_matrix_soft(semantics, terminals, avoid_constants=True, sim_f=torch.nn.functional.mse_loss, sim_th=0):
    """Builds approximate similarity matrix based on semantics.

    In practical terms, this function creates a pandas.DataFrame with
    semantically equivalent pairs of subtrees that will be used for the
    replacement algorithm. Each row stores a pair {T(s_i), T(s_j)},
    such that T(s_i) is larger than T(s_j), but both are semantically
    equivalent (in approximate terms).

    This function is dedicated for approximate simplification (i.e.,
    when the subtrees' semantics do not match exactly). In this case,
    the whole similarity matrix is stored and processed. The threshold
    represents the i-th percentile of the similarities' distribution.

    To accommodate for the floating-point rounding error, two semantics
    vectors are considered equal if the similarity between them, after
    being rounded to five decimals, equals zero.

    TODO: Join get_similarity_matrix_hard and get_similarity_matrix_soft
        into a single function. Also, confirm the avoid_constants.
        Later, this function will be replaced with get_similarity_matrix.

    Parameters
    ----------
    semantics : dict
        Dictionary that stores the semantics of each subtree. The
        terminal nodes are also regarded as individual subtrees.
        Each key represents an index of each subtree. Each value
        stores the respective children nodes one depth-level down
        and a torch.Tensor containing the semantics of the subtree.
    terminals : list
        List of integer indexes representing all the terminals nodes
         in a tree.
    avoid_constants : bool
        Ignore replacement of a three-node subtree if the subtree
        to replace with - T(Sj) - is a constant. This is relevant
        to avoid approximate simplifications that result in a single
        node tree that is a constant.
    sim_f : function
        A function of two inputs (x, y) that is used to compute the
        similarity between the semantics of two subtrees. The output
        of this function determines the semantic equivalency.
    sim_th : float
        The i-th percentile of the similarities' distribution that
        serves as a threshold for approximate semantics.

    Returns
    -------
    df_sim : pandas.DataFrame
        Data frame of similarity pairs.
    """
    sim_matrix, count_i = [], 0
    semantics_lst = list(semantics.keys())
    for x_i in range(0, len(semantics_lst), 1):
        nodes_x_i = []
        get_subtree_indices_(x_i, semantics, terminals, nodes_x_i)
        len_x_i = len(nodes_x_i)

        # Avoid computing similarity when Si is a terminal
        if len_x_i == 1:
            continue

        for x_j in range(x_i + 1, len(semantics_lst), 1):
            # Get semantics' shape
            si = semantics[x_i][-1].shape[0] if len(semantics[x_i][-1].shape) > 0 else 1
            sj = semantics[x_j][-1].shape[0] if len(semantics[x_j][-1].shape) > 0 else 1
            # Ignore replacement of a three-node subtree if the subtree to replace with T(Sj) is a constant
            if avoid_constants and (len_x_i == 3 and sj == 1 and si > 1):
                continue

            # Compute similarity
            sim_i_j = sim_f(semantics[x_i][-1].repeat(sj) if si < sj else semantics[x_i][-1],
                            semantics[x_j][-1].repeat(si) if sj < si else semantics[x_j][-1]).item()
            nodes_x_j = []
            get_subtree_indices_(x_j, semantics, terminals, nodes_x_j)
            len_x_j = len(nodes_x_j)
            # Skip iteration for equally sized subtrees
            if len_x_i == len_x_j:
                continue

            # Populate the list with the largest subtree first
            ordered_list = []
            x_i_smaller = len_x_i < len_x_j
            for i, j in [(x_i, x_j), (len(nodes_x_i), len(nodes_x_j)), (nodes_x_i, nodes_x_j)]:
                ordered_list.extend([j, i] if x_i_smaller else [i, j])

            # Append similarity
            ordered_list.insert(2, sim_i_j)
            # Append flag stating whether Si C Sj
            ordered_list.insert(3, set(nodes_x_i).issubset(set(nodes_x_j)) if x_i_smaller else set(nodes_x_j).issubset(set(nodes_x_i)))
            # Append length of semantics
            ordered_list.insert(4, si)
            ordered_list.insert(5, sj)
            # Append to the distance matrix
            sim_matrix.append(ordered_list)

    df_sim = pd.DataFrame(sim_matrix, columns=["Si", "Sj", "f(Si, Sj)", "Sj C Si", "len(Si)", "len(Sj)",
                                               "len(T(Si))", "len(T(Sj))", "T(Si)", "T(Sj)"])

    # Compute i-th percentile
    threshold = df_sim["f(Si, Sj)"].quantile(sim_th)
    # Filter based on the percentile
    df_sim = df_sim[df_sim["f(Si, Sj)"] <= threshold]

    return df_sim


def get_similarity_matrix(semantics, terminals, sim_f=torch.nn.functional.mse_loss, sim_th=0):
    """Builds similarity matrix based on semantics.

    In practical terms, this function creates a pandas.DataFrame with
    semantically equivalent pairs of subtrees that will be used for the
    replacement algorithm. Each row stores a pair {T(s_i), T(s_j)},
    such that T(s_i) is larger than T(s_j), but both are semantically
    equivalent.

    The function allows both exact and approximate simplification.

    To accommodate for the floating-point rounding error, two semantics
    vectors are considered equal if the similarity between them, after
    being rounded to five decimals, equals zero.

    TODO: Include approximate similarity based on several conditions:
        percentile, statistical test, etc.

    Parameters
    ----------
    semantics : dict
        Dictionary that stores the semantics of each subtree. The
        terminal nodes are also regarded as individual subtrees.
        Each key represents an index of each subtree. Each value
        stores the respective children nodes one depth-level down
        and a torch.Tensor containing the semantics of the subtree.
    terminals : list
        List of integer indexes representing all the terminals nodes in a tree.
    sim_f : function
        A function of two inputs (x, y) that is used to compute similarity between the
        semantics of two subtrees. The output of this function determines the semantic
        equivalency.
    sim_th : float
        Threshold on similarity approximation.

    Returns
    -------
    df_sim : pandas.DataFrame
        Data frame of similarity pairs.
    """
    sim_matrix, count_i = [], 0
    semantics_lst = list(semantics.keys())
    for x_i in range(0, len(semantics_lst), 1):
        # Get length of the nodes in a subtree, rooted at node x_i
        nodes_x_i = []
        get_subtree_indices_(x_i, semantics, terminals, nodes_x_i)
        len_x_i = len(nodes_x_i)

        # Avoid computing similarity when Si is a terminal, because there is no need to replace a terminal (size=1)
        if len_x_i == 1:
            continue

        for x_j in range(x_i + 1, len(semantics_lst), 1):
            # Get semantics' shape
            si = semantics[x_i][-1].shape[0] if len(semantics[x_i][-1].shape) > 0 else 1
            sj = semantics[x_j][-1].shape[0] if len(semantics[x_j][-1].shape) > 0 else 1

            # Compute similarity
            sim_i_j = sim_f(semantics[x_i][-1].repeat(sj) if si < sj else semantics[x_i][-1],
                            semantics[x_j][-1].repeat(si) if sj < si else semantics[x_j][-1]).item()

            # Rounds values close to zero:
            if round(sim_i_j, 5) == 0:  # sim_i_j = 0.000009 => 0
                nodes_x_j = []
                get_subtree_indices_(x_j, semantics, terminals, nodes_x_j)
                len_x_j = len(nodes_x_j)
                # Skip iteration for equally sized subtrees
                if len_x_i == len_x_j:
                    continue

                # Populate the list with the largest subtree first
                ordered_list = []
                x_i_smaller = len_x_i < len_x_j
                for i, j in [(x_i, x_j), (len_x_i, len_x_j), (nodes_x_i, nodes_x_j)]:
                    ordered_list.extend([j, i] if x_i_smaller else [i, j])

                # Append similarity
                ordered_list.insert(2, sim_i_j)
                # Append flag stating whether Si C Si
                ordered_list.insert(3, set(nodes_x_i).issubset(set(nodes_x_j)) if x_i_smaller else set(nodes_x_j).issubset(set(nodes_x_i)))
                # Append length of semantics
                ordered_list.insert(4, si)
                ordered_list.insert(5, sj)
                # Append the removed nodes
                # Append to the distance matrix
                sim_matrix.append(ordered_list)

        # Variance-based replacement rule
        constant_output = len(semantics[x_i][-1].shape) == 0
        if constant_output or round(semantics[x_i][-1].var().item(), 5) <= 0:
            len_semantics_x_i = semantics[x_i][-1].shape[0] if len(semantics[x_i][-1].shape) > 0 else 1
            replacement_constant = semantics[x_i][-1] if constant_output else semantics[x_i][-1].median()
            #
            sim_matrix.append([x_i, -1, 0, True, len_semantics_x_i, 1, len_x_i, 1, nodes_x_i, replacement_constant])

    df_sim = pd.DataFrame(sim_matrix, columns=["Si", "Sj", "f(Si, Sj)", "Sj C Si", "len(Si)", "len(Sj)",
                                               "len(T(Si))", "len(T(Sj))", "T(Si)", "T(Sj)"])
    return df_sim


def get_terminals(semantics):
    """Get indices of all terminal nodes in a tree.

    Parameters
    ----------
    semantics : dict
        Dictionary that stores the semantics of each subtree. The
        terminal nodes are also regarded as individual subtrees.
        Each key represents an index of each subtree. Each value
        stores the respective children nodes one depth-level down
        and a torch.Tensor containing the semantics of the subtree.

    Returns
    -------
    terminals : list
        List of integer indexes representing all the terminals nodes in a tree.
    """
    terminals = []
    for k, v in semantics.items():
        if len(v[0]) == 1 and k == v[0][0]:
            terminals.append(k)
    return terminals


def get_subtree_indices_(idx_subtree, semantics, terminals, idxs_spanning):
    """GP pruning based on semantics"""
    if idx_subtree in terminals:
        idxs_spanning.append(idx_subtree)
        return
    else:
        idxs_spanning.append(idx_subtree)
        branches = semantics[idx_subtree][0]
        for b in branches:
            get_subtree_indices_(b, semantics, terminals, idxs_spanning)
        return


def replace_subtrees(repr_, df_sim):
    """Performs subtrees replacement based on the similarity matrix.

    The replacement is performed in two ways:
        1) replace with constants
        2) replace with semantically equivalent subtrees

    Parameters
    ----------
    repr_ : list
        Representation of a GP tree as a list of program elements in
        prefix notation.
    df_sim : pandas.DataFrame
        Data frame of similarity pairs.

    Returns
    -------
    repr_new : list
        Simplified representation of a GP tree.
    """
    # Add column with removed nodes
    removed_indices = []
    for i, row in df_sim.iterrows():
        if isinstance(row["T(Sj)"], torch.Tensor):
            removed_indices.append(row["T(Si)"][1:])
        else:
            if row["T(Sj)"][0] in row["T(Si)"]: # If replacement is done within the subtree
                removed_indices.append(set(row["T(Si)"]) ^ set(row["T(Sj)"]))
            else:
                removed_indices.append(row["T(Si)"])

    df_sim["Removed"] = removed_indices

    # Shallow copy of the original representation
    repr_new = repr_[:]

    # Perform replacement of subtrees by constant first
    condition_constants = df_sim["Sj"] == -1
    while len(df_sim[condition_constants]) > 0:
        # Select the largest T_sub_i as a target for replacement
        idx_i = df_sim.loc[condition_constants, "len(T(Si))"].argmax()
        # Get index label of the largest T_sub_i in the dataframe
        loc_idx_i = df_sim.loc[condition_constants].index[idx_i]
        # Get index of the largest T_sub_i in the tree T
        si_largest = df_sim.loc[loc_idx_i, "Si"]
        # Get the constant to replace the tree with
        constant = df_sim.loc[loc_idx_i, "T(Sj)"]
        # Obtain indices of a subtree to be replaced: T(Si)
        to_replace_idx = df_sim.loc[df_sim["Si"] == si_largest, "T(Si)"].values[0]
        # Obtain indices of a subtree to replace with: T(Sj)
        to_add_idx = to_replace_idx
        # Get nodes of a subtree to replace
        to_replace = repr_new[to_replace_idx[0]: to_replace_idx[-1] + 1]
        # Get nodes of a subtree to add
        to_add = [constant] + [None for _ in range(len(to_add_idx) - 1)]
        # Replace
        for i in range(0, len(repr_new) - len(to_replace) + 1, 1):
            if repr_new[i: i + len(to_replace)] == to_replace:
                repr_new = repr_new[:i] + to_add + repr_new[i + len(to_replace):]
                break

        # Select a subset of entries regarding the replaced subtree Si
        condition_si_largest = df_sim["Si"] == si_largest
        # Select all subtrees contained in Si (i.e., Sj C Si) that are listed in the replacement rules
        removed_at_loc_idx_i = set(df_sim.loc[loc_idx_i, "Removed"])
        condition_contained_in_si = [True if set(rule).issubset(removed_at_loc_idx_i) else False for rule in df_sim["T(Si)"]]
        # Delete Sj C Si and delete the largest Si
        df_sim = df_sim[~((condition_si_largest) | (condition_contained_in_si))]
        # Update condition
        condition_constants = df_sim["Sj"] == -1

    # Shallow copy of the original representation
    repr_new_ = repr_new[:]
    # Perform replacement of subtrees by semantically equivalent subtrees
    while len(df_sim) > 0:
        # Select the largest T_sub_i as a target for replacement
        idx_i = df_sim.loc[:, "len(T(Si))"].argmax()
        # Get index label of the largest T_sub_i in the dataframe
        loc_idx_i = df_sim.index[idx_i]
        # Get index of the largest T_sub_i in the tree T
        si_largest = df_sim.loc[loc_idx_i, "Si"]
        # Select a subset of entries containing the largest T_sub_j
        condition_si_largest = df_sim["Si"] == si_largest
        # For that subset, select the smallest T_sub_j with compatible semantics for T_sub_i
        idx_j = df_sim.loc[condition_si_largest, "len(T(Sj))"].argmin()
        # Get index label of the largest T_sub_i in the dataframe
        loc_idx_j = df_sim.index[idx_j]
        # Get index of the smallest T_sub_j for T_sub_i in the tree T
        sj_smallest = df_sim.loc[loc_idx_j, "Sj"]
        # Obtain indices of a subtree to be replaced: T(Si)
        to_replace_idx = df_sim.loc[df_sim["Si"] == si_largest, "T(Si)"].values[0]
        # Obtain indices of a subtree to replace with: T(Sj)
        to_add_idx = df_sim.loc[df_sim["Sj"] == sj_smallest, "T(Sj)"].values[0]
        # Get nodes of a subtree to replace
        to_replace = repr_new_[to_replace_idx[0]: to_replace_idx[-1] + 1]
        # Get nodes of a subtree to add
        to_add = repr_new_[to_add_idx[0]: to_add_idx[-1] + 1]
        # Replace
        for i in range(0, len(repr_new) - len(to_replace) + 1, 1):
            if repr_new[i: i + len(to_replace)] == to_replace:
                repr_new = repr_new[:i] + to_add + repr_new[i + len(to_replace):]
                break

        # Select all subtrees contained in Si (i.e., Sj C Si)
        sj_c_si = df_sim.loc[(condition_si_largest) & (df_sim["Sj C Si"]), "Sj"]
        # Delete Sj C Si and delete the largest Si
        df_sim = df_sim[(~df_sim["Si"].isin(sj_c_si)) & (~condition_si_largest)]

    # Remove all Nones
    repr_new = list(filter(lambda item: item is not None, repr_new))

    return repr_new


def replace_subtrees_(repr_, df_sim):
    """Performs subtrees replacement based on the similarity matrix.

    The replacement accounts for semantically equivalent subtrees only.

    TODO: Later, this function will be replaced with replace_subtrees.

    Parameters
    ----------
    repr_ : list
        Representation of a GP tree as a list of program elements in
        prefix notation.
    df_sim : pandas.DataFrame
        Data frame of similarity pairs.

    Returns
    -------
    repr_new : list
        Simplified representation of a GP tree.
    """
    repr_new = repr_[:]  # shallow copy
    while len(df_sim) > 0:
        # Select the largest T_sub_i
        idx_i = df_sim.loc[:, "len(T(Si))"].argmax()
        si_largest = df_sim.iloc[idx_i, 0]
        # Select the smallest T_sub_j with compatible semantics for T_sub_i
        idx_j = df_sim.loc[df_sim["Si"] == si_largest, "len(T(Sj))"].argmin()
        sj_smallest = df_sim[df_sim["Si"] == si_largest].iloc[idx_j, 1]
        # Obtain indices of a subtree to be replaced: T(Si)
        to_replace = df_sim.loc[df_sim["Si"] == si_largest, "T(Si)"].values[0]
        # Obtain indices of a subtree to replace with: T(Sj)
        to_add = df_sim.loc[df_sim["Sj"] == sj_smallest, "T(Sj)"].values[0]
        # Get nodes of a subtree to replace
        to_replace = repr_[to_replace[0]: to_replace[-1] + 1]
        # Get nodes of a subtree to add
        to_add = repr_[to_add[0]: to_add[-1] + 1]

        # Replace
        for i in range(0, len(repr_new) - len(to_replace) + 1, 1):
            if repr_new[i: i + len(to_replace)] == to_replace:
                repr_new = repr_new[:i] + to_add + repr_new[i + len(to_replace):]
                break

        # Select all subtrees contained in Si (i.e., Sj C Si)
        sj_c_si = df_sim.loc[(df_sim["Si"] == si_largest) & (df_sim["Sj C Si"]), "Sj"]
        # Delete Sj C Si and delete the largest Si
        df_sim = df_sim[(~df_sim["Si"].isin(sj_c_si)) & (df_sim["Si"] != si_largest)]

    return repr_new


def _prune_tree_dict(repr_, X, avoid_constants=True, sim_f=torch.nn.functional.mse_loss, sim_th=0, re_run=True):
    """Performs tree simplification based on semantics X.

    1) Processes the input data tensor X using tree representation.
    2) Simultaneously, creates a dictionary containing the semantics of
        each sub-tree. Terminals are regarded as sub-trees.
    3) Generates a table containing replacement rules based on semantic
        similarity.
    4) Uses the rules generated in 3) to simplify the tree.
    5) In necessary, evaluates again the tree.

    Parameters
    ----------
    repr_ : list
        Representation of a GP tree as a list of program elements in
        prefix notation.
    X : torch.Tensor
        Input data.
    avoid_constants : bool
        Ignore replacement of a three-node subtree if the subtree
        to replace with - T(Sj) - is a constant.
    sim_f : function
        A function of two inputs (x, y) that is used to compute the
        similarity between the semantics of two subtrees. The output
        of this function determines the semantic equivalency.
    sim_th : float
        The i-th percentile of the similarities' distribution that
        serves as a threshold for approximate semantics.
    re_run : bool
        Whether or not to re-execute the simplified tree on X (i.e.,
        re-compute its output). Trees will be automatically re-executed
        if sim_th > 0. Additionally, one can use re_run=True when
        performing exact the simplification for debugging (i.e., to
        check whether exact simplification does not change the output).

    Returns
    -------
    intermediate_result : torch.Tensor
        The output vector of size len(X) generated by processing X using
         the tree representation.
    """
    node = repr_[0]
    # Secure against constants' tree
    if isinstance(node, torch.Tensor):
        if sim_th > 0 or re_run:
            return repr_, node.repeat_interleave(len(X)), node.repeat_interleave(len(X))
        else:
            return repr_, node.repeat_interleave(len(X))
    if isinstance(node, int):
        if sim_th > 0 or re_run:
            return repr_, X[:, node], X[:, node]
        else:
            return repr_, X[:, node]

    apply_stack = []
    # Semantics of the sub-trees {index of the sub-tree's root: [ [indices of nodes 1 depth below, semantics ] ]
    semantics = {}
    idx_node = 0  # indices to keep track nodes and semantics of the subtrees
    idx_subtree = []  # indices in repr_ to keep track of the sub-trees
    for node in repr_:  # idx represents an index of the node in the list
        if isinstance(node, _Function):
            apply_stack.append([node])  # appends a Function as a list containing it
            # S: adds a new entry for the subtrees
            semantics[idx_node] = [[], None]
            # S: appends the index of the root of a subtree
            idx_subtree.append(idx_node)
            # S: counts the index of the node
            idx_node += 1
        else:
            apply_stack[-1].append(node)  # appends the respective terminals to the list containing the Function
            # S: Adds a special subtree: single terminal
            semantics[idx_node] = [[idx_node], X[:, node] if isinstance(node, int) else node]
            # S: Appends the index of the terminal
            semantics[idx_subtree[-1]][0].append(idx_node)
            # S: counts the index of the node
            idx_node += 1

        while len(apply_stack[-1]) == apply_stack[-1][0].arity + 1:
            function_ = apply_stack[-1][0]
            terminals = [X[:, t] if isinstance(t, int) else t for t in apply_stack[-1][1:]]
            intermediate_result = function_(*terminals)
            if len(apply_stack) != 1:
                apply_stack.pop()
                apply_stack[-1].append(intermediate_result)
                # S: appends the semantics of the calculated subtree
                semantics[idx_subtree[-1]][-1] = intermediate_result
                # S: semantics for a given subtree were calculated, remove the index from the list
                semantics_from = idx_subtree.pop()
                if len(semantics[idx_subtree[-1]][0]) < apply_stack[-1][0].arity + 1:
                    semantics[idx_subtree[-1]][0].append(semantics_from)
            else:
                # S: add final output semantics
                semantics[idx_subtree[-1]][-1] = intermediate_result

                # 1) Obtains terminals from semantics
                terminals = get_terminals(semantics)
                # 2) Computes semantics similarity matrix S
                if sim_th == 0:
                    #df_sim = get_similarity_matrix_hard(semantics, terminals, sim_f)
                    df_sim = get_similarity_matrix(semantics, terminals, sim_f, 0)
                else:
                    df_sim = get_similarity_matrix_soft(semantics, terminals, avoid_constants, sim_f, sim_th)

                # 4) If there are entries in S, perform simplification
                if len(df_sim) > 0:
                    repr_new = replace_subtrees(repr_, df_sim)
                    if sim_th > 0 or re_run:
                        intermediate_result_new = _execute_tree(repr_new, X)

                        # Secure against single-node constant trees
                        if len(intermediate_result_new.shape) == 0:
                            intermediate_result_new = torch.cat(X.shape[0] * [intermediate_result_new[None]])

                        return repr_new, intermediate_result, intermediate_result_new
                    else:
                        return repr_new, intermediate_result

                else:
                    # Secure against single-node constant trees
                    if len(intermediate_result.shape) == 0:
                        intermediate_result = torch.cat(X.shape[0] * [intermediate_result[None]])

                    if sim_th > 0 or re_run:
                        return repr_, intermediate_result, None
                    else:
                        return repr_, intermediate_result

    return None


# +++++++++++++++++++++++++++ GSGP reconstruction algorithm
def prm_reconstruct_tree(history, path_init_pop, path_rts, device="cpu"):
    """ Implements GSGP trees' reconstruction

    This function is used to provide the reconstruct_tree (the inner
    function) with the necessary environment (the outer scope) - the
    table which stores the evolution's history, the paths towards the
    initial and the random trees, and the processing device that was
    used in the underlying experiment.

    Parameters
    ----------
    history : pandas.DataFrame
        Stores the evolution's history in the following columns:
            - "Iter": iteration's number;
            - "Operator": the variation operator that was applied on
             a given offspring;
            - "T1": the ID of the first parent;
            - "T2": the ID of the second parent (if GSC was applied);
            - "Tr": the ID of a random tree generated (assumes only
             one random tree is necessary to apply an operator);
            - "ms": mutation's step (if GSM was applied);
            - "Fitness": offspring's training fitness;
    path_init_pop : str
        Paths towards the initial trees.
    path_rts : str
        Paths towards the random trees.
    device : str (default="cpu")
        Specification of the processing device that was used in the
        underling experiment.

    Returns
    -------
    reconstruct_tree : function
        A function that reconstructs the user-specified tree.
    """
    # Verifies initial trees' directory
    if not os.path.isdir(path_init_pop):
        raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), path_init_pop)
    # Verifies random trees' directory
    if not os.path.isdir(path_rts):
        raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), path_rts)

    def reconstruct_tree(id):
        """ Implements GSGP trees' reconstruction

        Reconstructs the user-specified tree following its evolutionary
        history.

        Parameters
        ----------
        id : str
            Row's index in history pandas.DataFrame, which serves as an
            identification of the tree to be reconstructed.

        Returns
        -------
        list
            GSGP tree stored as a LISP tree.
        """
        if history.loc[id, "Iter"] == 1:
            if history.loc[id, "Operator"] == "crossover":
                # Loads trees
                with open(os.path.join(path_init_pop, history.loc[id, "T1"] + '.pickle'), 'rb') as f:
                    t1 = pickle.load(f)
                with open(os.path.join(path_init_pop, history.loc[id, "T2"] + '.pickle'), 'rb') as f:
                    t2 = pickle.load(f)
                with open(os.path.join(path_rts, history.loc[id, "Tr"] + '.pickle'), 'rb') as f:
                    tr = pickle.load(f)
                # Returns as geometric semantic crossover
                return [add2, mul2] + tr + t1 + [mul2, sub2, torch.tensor([1.0], device=device)] + tr + t2
            else:
                # Loads trees
                with open(os.path.join(path_init_pop, history.loc[id, "T1"] + '.pickle'), 'rb') as f:
                    t1 = pickle.load(f)
                with open(os.path.join(path_rts, history.loc[id, "Tr"] + '.pickle'), 'rb') as f:
                    tr = pickle.load(f)
                ms = torch.tensor([history.loc[id, "ms"]], device=device)

                # Returns as geometric semantic mutation
                return [add2] + t1 + [mul2, ms] + tr
        else:
            if history.loc[id, "Operator"] == "crossover":
                # Loads the random tree
                with open(os.path.join(path_rts, history.loc[id, "Tr"] + '.pickle'), 'rb') as f:
                    tr = pickle.load(f)
                # Returns as geometric semantic crossover recursively
                return [add2, mul2]+tr+reconstruct_tree(history.loc[id, "T1"]) + \
                       [mul2, sub2, torch.tensor([1.0], device=device)]+tr+reconstruct_tree(history.loc[id, "T2"])
            else:
                # Loads the random tree
                with open(os.path.join(path_rts, history.loc[id, "Tr"] + '.pickle'), 'rb') as f:
                    tr = pickle.load(f)
                ms = torch.tensor([history.loc[id, "ms"]], device=device)

                # Returns as geometric semantic mutation
                return [add2]+reconstruct_tree(history.loc[id, "T1"])+[mul2, ms]+tr

    return reconstruct_tree
