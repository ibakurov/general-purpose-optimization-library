{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "combined-magazine",
   "metadata": {},
   "source": [
    "# Solving combinatorial optimization problems.\n",
    "When solving an instance of a combinatorial optimization problem, one is generally interested in an object from a finite or, possibly, a countable infinite set that satisfies certain conditions or constraints. Such an object can be an integer, a set, a permutation, or a graph.\n",
    "This notebook shows how to define and solve an instance of a travelling salesman problem (TSP).\n",
    "\n",
    "## TSP.\n",
    "The TSP is a popular NP-hard combinatorial problem, of high importance in theoretical and applied computer science and operations research. It inspired several other important problems, such as vehicle routing and travelling purchaser. In simple terms, the travelling salesman must visit every city in a given region exactly once and then return to the starting point. The problem proposes the following question: given the cost of travel between all the cities, how should the itinerary be planned to minimize the total cost of the entire tour.\n",
    "\n",
    "<img src=\"https://i.pinimg.com/originals/b0/95/4c/b0954c93b3c37876e9b54736453a9d5c.jpg\" alt=\"Drawing\" style=\"width: 400px;\"/>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "focal-billion",
   "metadata": {},
   "source": [
    "# 1. Create problems' instances."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "faced-titanium",
   "metadata": {},
   "source": [
    "Loads the necessary classes and functions."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "id": "sufficient-pound",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Imports PyTorch\n",
    "import torch\n",
    "# Imports problems\n",
    "from gpol.problems.tsp import TSP\n",
    "from gpol.utils.utils import travel_distance\n",
    "# Imports metaheuristics \n",
    "from gpol.algorithms.random_search import RandomSearch\n",
    "from gpol.algorithms.local_search import HillClimbing\n",
    "from gpol.algorithms.local_search import SimulatedAnnealing\n",
    "from gpol.algorithms.genetic_algorithm import GeneticAlgorithm\n",
    "# Imports operators\n",
    "from gpol.operators.initializers import rnd_vshuffle, rnd_mshuffle\n",
    "from gpol.operators.selectors import prm_tournament\n",
    "from gpol.operators.variators import partially_mapped_xo, prm_iswap_mnt"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "numeric-celtic",
   "metadata": {},
   "source": [
    "Creates an instance of ``TSP``. The search space (*S*) of an instance of ``TSP`` consists of the following key-value pairs:\n",
    "- ``\"distances\"`` is a $n$x$n$ tensor of type ``torch.float`` which represents the distances' matrix for the underlying problem. The matrix can be either symmetric or asymmetric. A given row $i$ in the matrix represents the set of distances between $i$, as being the origin, and the $n$ possible destinations (including $i$ itself);\n",
    "- ``\"origin\"``: an integer value representing the origin, i.e., the point from where the *travelling salesman* departs."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "id": "disciplinary-mattress",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Defines the processing device and random state 's seed\n",
    "device, seed = \"cpu\", 0  # 'cuda' if torch.cuda.is_available() else 'cpu', 0\n",
    "# Characterizes the problem: distance matrix (from https://developers.google.com/optimization/routing/tsp)\n",
    "dist_mtrx = torch.tensor([[0, 2451, 713, 1018, 1631, 1374, 2408, 213, 2571, 875, 1420, 2145, 1972],\n",
    "                [2451, 0, 1745, 1524, 831, 1240, 959, 2596, 403, 1589, 1374, 357, 579],\n",
    "                [713, 1745, 0, 355, 920, 803, 1737, 851, 1858, 262, 940, 1453, 1260],\n",
    "                [1018, 1524, 355, 0, 700, 862, 1395, 1123, 1584, 466, 1056, 1280, 987],\n",
    "                [1631, 831, 920, 700, 0, 663, 1021, 1769, 949, 796, 879, 586, 371],\n",
    "                [1374, 1240, 803, 862, 663, 0, 1681, 1551, 1765, 547, 225, 887, 999],\n",
    "                [2408, 959, 1737, 1395, 1021, 1681, 0, 2493, 678, 1724, 1891, 1114, 701],\n",
    "                [213, 2596, 851, 1123, 1769, 1551, 2493, 0, 2699, 1038, 1605, 2300, 2099],\n",
    "                [2571, 403, 1858, 1584, 949, 1765, 678, 2699, 0, 1744, 1645, 653, 600],\n",
    "                [875, 1589, 262, 466, 796, 547, 1724, 1038, 1744, 0, 679, 1272, 1162],\n",
    "                [1420, 1374, 940, 1056, 879, 225, 1891, 1605, 1645, 679, 0, 1017, 1200],\n",
    "                [2145, 357, 1453, 1280, 586, 887, 1114, 2300, 653, 1272, 1017, 0, 504],\n",
    "                [1972, 579, 1260, 987, 371, 999, 701, 2099, 600, 1162, 1200, 504, 0]], dtype=torch.float, device=device)\n",
    "\n",
    "# Creates the search space\n",
    "sspace_tsp = {\"distances\": dist_mtrx, \"origin\": 3}\n",
    "# Creates an instance of TSP\n",
    "pi_tsp = TSP(sspace=sspace, ffunction=travel_distance, min_=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "urban-diary",
   "metadata": {},
   "source": [
    "# 2. Choose and parametrize the algorithms."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "destroyed-clear",
   "metadata": {},
   "source": [
    "## 2.1. Random search (RS).\n",
    "The random search (RS) can be seen as thethe first rudimentary stochastic metaheuristic for problem-solving. Its strategy, far away from being *intelligent*, consists of randomly sampling $S$ for a given number of iterations. As such, the only search-parameter of an instance of ``RandomSearch`` is the initialization function (the ``initializer``). The function ``rnd_vshuffle`` is one of the numerous initializers for the single-point (SP) algorithms and it randomly shuffles *cities*' indexes (which are defined in the $S$ of the problem's instance)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "sharp-sunglasses",
   "metadata": {},
   "source": [
    "The cell in below creates two dictionaries, called ``pars`` which stores algorithms' parameters for each problem type. Each key-value pair stores the algorithm's type and a dictionary of respective search-parameters for a given problem. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "id": "published-cathedral",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Defines single-point (SP) initializers for each problem\n",
    "sp_init = rnd_vshuffle\n",
    "# Defines RS's parameters\n",
    "pars = {RandomSearch: {\"initializer\": sp_init}}"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "cathedral-omaha",
   "metadata": {},
   "source": [
    "## 2.2. Hill climbing (HC).\n",
    "The local search (LS) algorithms can be seen among the first intelligent search strategies that improve the functioning of the RS. They rely upon the concept of neighborhood which is explored at each iteration by sampling from $S$ a limited number of neighbors of the best-so-far solution. Usually, the LS algorithms are divided in two branches. In the first branch, called hill climbing (HC), or hill descent for the minimization problems, the best-so-far solution is replaced by its neighbor when the latter is at least as good as the former."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "israeli-engagement",
   "metadata": {},
   "source": [
    "The cell in below adds ``HillClimbing`` to ``pars``. Note that, unlike it was for ``RandomSearch``, an instance of ``HillClimbing`` also requires the specification of a neighbor-generation function (``\"nh_function\"``) and the neighborhood's size (``\"nh_size\"``). In this example, the so-called *swap-mutation* is used for the TSP, with a probability of swapping a random indexes' pair equal to $0.3$\\%. Note that the very same initialization function is used for both ``RandomSearch`` and ``HillClimbing``. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "id": "august-cheese",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Defines the size of the neighborhood \n",
    "nh_size = 100\n",
    "# Defines neighbor-generation function with the respective parameters\n",
    "nh_function = prm_iswap_mnt(prob=0.3)\n",
    "# Defines HC's parameters\n",
    "pars[HillClimbing] = {\"initializer\": sp_init, \"nh_function\": nh_function, \"nh_size\": nh_size}"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "medieval-tractor",
   "metadata": {},
   "source": [
    "## 2.3. Simulated annealing (SA).\n",
    "The second branch, called simulated annealing (SA), extends HC by formulating a non-negative probability of replacing the best-so-far solution by its neighbor when the latter is worse. Traditionally, such a probability is small and decreases as the search advances. The strategy adopted by SA is especially useful when the search is prematurely tagnated at a locally sub-optimal point in $S$."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "protecting-frederick",
   "metadata": {},
   "source": [
    "The cell in below adds ``SimulatedAnnealing`` to ``pars``. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 22,
   "id": "related-surgery",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Defines SA's parameters\n",
    "pars[SimulatedAnnealing] = {\"initializer\": sp_init, \"nh_function\": nh_function, \"nh_size\": nh_size, \"control\": 1.0, \"update_rate\": 0.9}"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "looking-horizontal",
   "metadata": {},
   "source": [
    "## 2.4. Genetic algorithm (GA).\n",
    "Based on the number of candidate solutions they handle at each step, the metaheuristics can be categorized into single-point (SP) and population-based (PB) approaches. \n",
    "The search procedure in the SP metaheuristics is generally guided by the information provided by a single candidate solution from $S$, usually the best-so-far solution, that is gradually evolved in a well-defined manner in hope to find the global optimum. The abovementioned HC and SA are examples of SP metaheuristics as the search is performed by exploring the neighborhood $N(i)$, where $i$ is the current best solution. Contrarily, the search procedure in PB metaheuristics is generally guided by the information shared by a set of candidate solutions and the exploitation of its collective behavior in different ways. In abstract terms, one can say that every PB metaheuristics shares, at least, the following two features: an object representing the set of simultaneously exploited candidate solutions (i.e., the population), and a procedure to *move* them across $S$.\n",
    "\n",
    "Genetic Algorithm (GAs) is a meta-heuristic introduced by J. Holland which was strongly inspired by Darwin's theory of evolution by means of natural selection. Conceptually, the algorithm starts with a random-like population of candidate solutions (called *chromosomes*). Then, by mimicking the natural selection and genetically inspired variation operators, such as the crossover and the mutation, the algorithm breeds a population of the next-generation candidate solutions (called the *offspring population*), that replaces the previous population (a.k.a. the *parent population*). This procedure is iterated until reaching some stopping criteria, like a maximum\n",
    "number of iterations (also called *generations*)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "expanded-pastor",
   "metadata": {},
   "source": [
    "The cell in below adds ``GeneticAlgorithm`` to ``pars``. It uses a slightly different initializer, specially designed to efficiently initialize PB metaheuristics, called ``rnd_mshuffle``, that shuffles a collection of cities' vectors organized in a matrix. Note that the mutation function is the neighbor-generation function, that was used for the aforementioned LS algorithms, and the population's size is equivalent to the neighborhood's size; this is done to foster the equivalency between LS and PB metaheuristics. Finally, the crossover is partially mapped, the elite is always a member of the population and parents' reproduction is enabled (``elitism=True`` and ``reproduction=True``)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 23,
   "id": "manufactured-sally",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Defines a population-based (PB) initializer\n",
    "pb_init = rnd_mshuffle\n",
    "# Defines GA's parameters\n",
    "pars[GeneticAlgorithm] = {\"pop_size\": nh_size, \"initializer\": pb_init, \"selector\": prm_tournament(pressure=0.05), \"mutator\": nh_function, \n",
    "                          \"crossover\": partially_mapped_xo, \"p_m\": 0.3, \"p_c\": 0.7, \"elitism\": True, \"reproduction\": True}"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "arbitrary-darkness",
   "metadata": {},
   "source": [
    "# 3. Executes the experiment."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "palestinian-movie",
   "metadata": {},
   "source": [
    "Note that *many* parameters and functions are shared across different algorithms in the experiment. This allows to increase the control and comparability between different algorithmic approaches when solving a given problem's instance."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "selected-interpretation",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "<class 'gpol.algorithms.random_search.RandomSearch'>\n",
      "\t initializer <function rnd_vshuffle at 0x00000292A35428B0>\n",
      "<class 'gpol.algorithms.local_search.HillClimbing'>\n",
      "\t initializer <function rnd_vshuffle at 0x00000292A35428B0>\n",
      "\t nh_function <function prm_iswap_mnt.<locals>.iswap_mnt at 0x00000292A3542F70>\n",
      "\t nh_size 100\n",
      "<class 'gpol.algorithms.local_search.SimulatedAnnealing'>\n",
      "\t initializer <function rnd_vshuffle at 0x00000292A35428B0>\n",
      "\t nh_function <function prm_iswap_mnt.<locals>.iswap_mnt at 0x00000292A3542F70>\n",
      "\t nh_size 100\n",
      "\t control 1\n",
      "\t update_rate 0.95\n",
      "<class 'gpol.algorithms.genetic_algorithm.GeneticAlgorithm'>\n",
      "\t pop_size 100\n",
      "\t initializer <function rnd_mshuffle at 0x00000292A3542940>\n",
      "\t selector <function prm_tournament.<locals>.tournament at 0x00000292A3568E50>\n",
      "\t mutator <function prm_iswap_mnt.<locals>.iswap_mnt at 0x00000292A3542F70>\n",
      "\t crossover <function partially_mapped_xo at 0x00000292A3538B80>\n",
      "\t p_m 0.5\n",
      "\t p_c 0.5\n",
      "\t elitism True\n",
      "\t reproduction False\n"
     ]
    }
   ],
   "source": [
    "for isa_type, isa_pars in pars.items():\n",
    "    print(isa_type)\n",
    "    for p_name, p_val in isa_pars.items():\n",
    "        print(\"\\t\", p_name, p_val)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "incorporated-success",
   "metadata": {},
   "source": [
    "Defines the computational resources for the experiment: the number of iterations."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 24,
   "id": "derived-emission",
   "metadata": {},
   "outputs": [],
   "source": [
    "n_iter = 30"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "resident-carolina",
   "metadata": {},
   "source": [
    "Loops the afore-defined ``pars`` dictionary containing algorithms' and the underlying parameters. Note that besides algorithm-specific parameters, the constructor of an instance of a search algorithm also receives the random state to initialize a pseudorandom number generator (called ``seed``), and the specification of the processing ``device`` (either CPU or GPU).\n",
    "\n",
    "The ``solve`` method has the same signature for all the search algorithms and, in this example, includes the following parameters: \n",
    "-  ``n_iter``: number of iterations to conduct the search;\n",
    "-  ``tol``: minimum required fitness improvement for ``n_iter_tol`` consecutive iterations to continue the search. When the fitness is not improving by at least ``tol`` for ``n_iter_tol`` consecutive iterations, the search will be automatically interrupted;\n",
    "-  ``n_iter_tol``: maximum number of iterations to not meet ``tol`` improvement;\n",
    "-  ``verbose``: verbosity's detail-level;\n",
    "-  ``log``: log-files' detail-level (if exists)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 25,
   "id": "offshore-logic",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "-------------------------------------------------\n",
      "           |           Best solution            |\n",
      "-------------------------------------------------\n",
      "Generation   Length   Fitness              Timing\n",
      "0            12       16524                 0.002\n",
      "1            12       14459                 0.000\n",
      "2            12       14459                 0.000\n",
      "3            12       14459                 0.000\n",
      "4            12       14459                 0.000\n",
      "5            12       14459                 0.000\n",
      "6            12       14459                 0.000\n",
      "Algorithm: RandomSearch\n",
      "Best solution's fitness: 14459.000\n",
      "Best solution: tensor([ 2,  4, 12,  5,  1, 10,  7,  0,  6,  8,  9, 11])\n",
      "\n",
      "--------------------------------------------------------------------------------------\n",
      "           |           Best solution              |           Neighborhood           |\n",
      "--------------------------------------------------------------------------------------\n",
      "Generation | Length   Fitness              Timing | AVG Fitness           STD Fitness\n",
      "0          | 12       16524                 0.000 | -1                             -1\n",
      "1          | 12       12706                 0.012 | 16289.1                    1620.1\n",
      "2          | 12       11201                 0.010 | 15076.2                   2057.31\n",
      "3          | 12       10178                 0.010 | 14683.2                   2138.68\n",
      "4          | 12       9921                  0.009 | 14010.2                   2265.34\n",
      "5          | 12       8727                  0.009 | 14260.5                   2452.66\n",
      "6          | 12       8262                  0.010 | 13870.3                   2631.28\n",
      "7          | 12       8058                  0.010 | 13907.6                   2653.94\n",
      "8          | 12       8058                  0.010 | 13290.7                   2723.21\n",
      "9          | 12       8016                  0.010 | 13736.8                   2870.68\n",
      "10         | 12       8016                  0.010 | 13455                     2906.74\n",
      "11         | 12       7861                  0.009 | 12996.5                    2769.2\n",
      "12         | 12       7861                  0.009 | 13716.5                   2832.09\n",
      "13         | 12       7861                  0.009 | 13134.3                   3021.35\n",
      "14         | 12       7861                  0.009 | 13700.9                   2664.71\n",
      "15         | 12       7861                  0.011 | 13554.9                   2447.27\n",
      "16         | 12       7861                  0.009 | 13352.6                    2606.7\n",
      "Algorithm: HillClimbing\n",
      "Best solution's fitness: 7861.000\n",
      "Best solution: tensor([ 9,  2,  7,  0, 10,  5, 12,  6,  8,  1, 11,  4])\n",
      "\n",
      "--------------------------------------------------------------------------------------\n",
      "           |           Best solution              |           Neighborhood           |\n",
      "--------------------------------------------------------------------------------------\n",
      "Generation | Length   Fitness              Timing | AVG Fitness           STD Fitness\n",
      "0          | 12       16524                 0.000 | -1                             -1\n",
      "1          | 12       12706                 0.010 | 16289.1                    1620.1\n",
      "2          | 12       11201                 0.012 | 15076.2                   2057.31\n",
      "3          | 12       10178                 0.010 | 14683.2                   2138.68\n",
      "4          | 12       9921                  0.009 | 14010.2                   2265.34\n",
      "5          | 12       8727                  0.011 | 14260.5                   2452.66\n",
      "6          | 12       8262                  0.011 | 13870.3                   2631.28\n",
      "7          | 12       8058                  0.012 | 13907.6                   2653.94\n",
      "8          | 12       8058                  0.016 | 13290.7                   2723.21\n",
      "9          | 12       8016                  0.008 | 13674.2                   2748.78\n",
      "10         | 12       8016                  0.009 | 13484.6                   2720.02\n",
      "11         | 12       8016                  0.008 | 13131.2                   2657.22\n",
      "12         | 12       8016                  0.009 | 13430.4                   2506.52\n",
      "13         | 12       8016                  0.008 | 13088.8                   2725.18\n",
      "14         | 12       8016                  0.009 | 13711.8                   2588.03\n",
      "Algorithm: SimulatedAnnealing\n",
      "Best solution's fitness: 8016.000\n",
      "Best solution: tensor([ 9,  2,  7,  0,  5, 10, 12,  6,  8,  1, 11,  4])\n",
      "\n",
      "--------------------------------------------------------------------------------------\n",
      "           |           Best solution              |            Population            |\n",
      "--------------------------------------------------------------------------------------\n",
      "Generation | Length   Fitness              Timing | AVG Fitness           STD Fitness\n",
      "0          | 12       10735                 0.003 | 16096.1                   1967.84\n",
      "1          | 12       10735                 0.072 | 14804.2                   1950.52\n"
     ]
    },
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "C:\\Miniconda\\envs\\gpol_test\\lib\\site-packages\\gpol\\operators\\variators.py:459: UserWarning: This overload of nonzero is deprecated:\n",
      "\tnonzero()\n",
      "Consider using one of the following signatures instead:\n",
      "\tnonzero(*, bool as_tuple) (Triggered internally at  ..\\torch\\csrc\\utils\\python_arg_parser.cpp:882.)\n",
      "  o1[idxs_out_[out]] = o2[idxs_in_][(o1[idxs_out_[out]] == o1[idxs_in_]).nonzero()[0]]\n"
     ]
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "2          | 12       10735                 0.067 | 13817.3                   1985.15\n",
      "3          | 12       10206                 0.050 | 13059.9                   1997.49\n",
      "4          | 12       9227                  0.049 | 12195                     1832.25\n",
      "5          | 12       9227                  0.048 | 12315.5                   2283.86\n",
      "6          | 12       9148                  0.041 | 11524.4                   2226.34\n",
      "7          | 12       9148                  0.038 | 11252.2                   2483.42\n",
      "8          | 12       8785                  0.042 | 11058.9                   2378.56\n",
      "9          | 12       8455                  0.039 | 10690.3                   2592.47\n",
      "10         | 12       8455                  0.035 | 10737.7                   2904.01\n",
      "11         | 12       8455                  0.038 | 10860.1                   3118.62\n",
      "12         | 12       8455                  0.038 | 9997.22                   2348.86\n",
      "13         | 12       8455                  0.032 | 10256.8                    2724.7\n",
      "14         | 12       8455                  0.031 | 10232.2                   3124.55\n",
      "Algorithm: GeneticAlgorithm\n",
      "Best solution's fitness: 8455.000\n",
      "Best solution: tensor([ 4, 12, 11,  1,  8,  6,  2,  7,  0,  9, 10,  5])\n",
      "\n"
     ]
    }
   ],
   "source": [
    "for isa_type, isa_pars in pars.items():\n",
    "    isa = isa_type(pi=pi, **isa_pars, seed=seed, device=device)\n",
    "    # n_iter*pop_size if isinstance(isa, RandomSearch) else n_iter  # equivalency for the RS\n",
    "    isa.solve(n_iter=n_iter, tol=20, n_iter_tol=5, verbose=2, log=0)\n",
    "    print(\"Algorithm: {}\".format(isa_type.__name__))\n",
    "    print(\"Best solution's fitness: {:.3f}\".format(isa.best_sol.fit))\n",
    "    print(\"Best solution:\", isa.best_sol.repr_, end=\"\\n\\n\")"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
