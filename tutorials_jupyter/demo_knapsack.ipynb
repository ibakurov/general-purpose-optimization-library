{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "architectural-director",
   "metadata": {},
   "source": [
    "# Solving combinatorial optimization problems.\n",
    "When solving an instance of a combinatorial optimization problem, one is generally interested in an object from a finite or, possibly, a countable infinite set that satisfies certain conditions or constraints. Such an object can be an integer, a set, a permutation, or a graph.\n",
    "This notebook shows how to define and solve an instance of a knapsack problem.\n",
    "\n",
    "## Knapsack.\n",
    "The knapsack is another popular combinatorial problem in the scientific community. In a knapsack problem, one is given a set of items, each associated to a given value and size (such as the weight and/or volume), and a *knapsack* with a maximum capacity; solving an instance of a knapsack problem implies to *pack* a subset of items into the knapsack, so that the items’ total size does not exceed the knapsack’s capacity, and their total value is maximized. If the total size of the items exceeds the capacity, such a solution is considered unfeasible. There is a wide range of knapsack-based problems, many of which are NP-hard, and large instances of such problems can be approached only by using heuristic algorithms. In this release, the library contains two variants: the *0-1* and the *bounded* knapsack problems, implemented in the module ``knapsack`` as classes ``Knapsack01`` and ``KnapsackBounded``, respectively.\n",
    "\n",
    "<img src=\"https://upload.wikimedia.org/wikipedia/commons/thumb/f/fd/Knapsack.svg/1200px-Knapsack.svg.png\" alt=\"Drawing\" style=\"width: 400px;\"/>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "outstanding-chance",
   "metadata": {},
   "source": [
    "# 1. Create problems' instances."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "numerous-swedish",
   "metadata": {},
   "source": [
    "Loads the necessary classes and functions."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 40,
   "id": "collective-sixth",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Imports PyTorch\n",
    "import torch\n",
    "# Imports problems\n",
    "from gpol.problems.knapsack import KnapsackBounded\n",
    "# Imports metaheuristics \n",
    "from gpol.algorithms.random_search import RandomSearch\n",
    "from gpol.algorithms.local_search import HillClimbing\n",
    "from gpol.algorithms.local_search import SimulatedAnnealing\n",
    "from gpol.algorithms.genetic_algorithm import GeneticAlgorithm\n",
    "# Imports operators\n",
    "from gpol.operators.initializers import prm_rnd_vint, prm_rnd_mint\n",
    "from gpol.operators.selectors import prm_tournament\n",
    "from gpol.operators.variators import prm_rnd_int_ibound, one_point_xo"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "crude-parent",
   "metadata": {},
   "source": [
    "Creates an instance of ``KnapsackBounded``. The search space (*S*) of an instance of ``KnapsackBounded`` consists of the following key-value pairs:\n",
    "- ``\"capacity\"`` as the maximum capacity of the *knapsack*;\n",
    "- ``\"n_dims\"`` as the number of unique items in $S$;\n",
    "- ``\"weights\"`` as the collection of items’ weights defined as a vector of type ``torch.float``;\n",
    "-  ``\"values\"`` as the collection of items’ values defined as a vector of type ``torch.float``; and \n",
    "-  ``\"bounds\"`` as a $2$x$n$ tensor representing the minimum and the maximum number of copies allowed for each of the $n$ items in $S$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 41,
   "id": "going-briefs",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Defines the processing device and random state 's seed\n",
    "device, seed =  \"cpu\", 0 # 'cuda' if torch.cuda.is_available() else 'cpu', 0\n",
    "# Characterizes the problem: defines the maximum number of items and their maximum allowed repetitions\n",
    "n_items, max_rep = 17, 4\n",
    "# Randomly generates items ’ weights and values (for the sake of example)\n",
    "torch.manual_seed (seed)  # sets the random state \n",
    "weights = torch.FloatTensor(n_items).uniform_(1, 9).to(device)\n",
    "values = torch.FloatTensor(n_items).uniform_(0.5 , 20).to(device)\n",
    "bounds = torch.stack((torch.ones(n_items), max_rep*torch.ones(n_items))).to(device)  # 1-4 copies per item\n",
    "# Creates the search space\n",
    "sspace = {\"capacity\": 160, \"n_dims\": n_items, \"weights\": weights, \"values\": values, \"bounds\": bounds}\n",
    "# Creates an instance of KnapsackBounded\n",
    "pi = KnapsackBounded(sspace=sspace, ffunction=torch.matmul, min_=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "adult-america",
   "metadata": {},
   "source": [
    "# 2. Choose and parametrize the algorithms."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "retained-depression",
   "metadata": {},
   "source": [
    "## 2.1. Random search (RS).\n",
    "The random search (RS) can be seen as thethe first rudimentary stochastic metaheuristic for problem-solving. Its strategy, far away from being *intelligent*, consists of randomly sampling $S$ for a given number of iterations. As such, the only search-parameter of an instance of ``RandomSearch`` is the initialization function (the ``initializer``). The function ``prm_rnd_vint`` returns a vector which follows a discrete uniform distribution between with user-specified lower and upper bounds."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "little-mumbai",
   "metadata": {},
   "source": [
    "The cell in below creates a dictionary called ``pars`` which stores algorithms' parameters. Each key-value pair stores the algorithm's type and a dictionary of respective search-parameters. The first key-value pair regards the RS."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 42,
   "id": "employed-strike",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Defines a single-point (SP) initializer\n",
    "sp_init = prm_rnd_vint(lb=0, ub=max_rep+1)  # + 1 because ~U{lb, ub}, with ub excluded \n",
    "# Defines RS's parameters\n",
    "pars = {RandomSearch: {\"initializer\": sp_init}}"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "mighty-killer",
   "metadata": {},
   "source": [
    "## 2.2. Hill climbing (HC).\n",
    "The local search (LS) algorithms can be seen among the first intelligent search strategies that improve the functioning of the RS. They rely upon the concept of neighborhood which is explored at each iteration by sampling from $S$ a limited number of neighbors of the best-so-far solution. Usually, the LS algorithms are divided in two branches. In the first branch, called hill climbing (HC), or hill descent for the minimization problems, the best-so-far solution is replaced by its neighbor when the latter is at least as good as the former."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "decimal-midnight",
   "metadata": {},
   "source": [
    "The cell in below adds ``HillClimbing`` to ``pars``. Note that, unlike it was for ``RandomSearch``, an instance of ``HillClimbing`` also requires the specification of a neighbor-generation function (``\"nh_function\"``) and the neighborhood's size (``\"nh_size\"``). In this example, a randomly selected index is replaced in the candidate solution, with a probability of $0.3$; the replacement value is a randomly drawn integer in the range [0, ``max_rep``]. Note that the very same initialization function is used for both ``RandomSearch`` and ``HillClimbing``. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 43,
   "id": "inside-medium",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Defines the size of the population/neighborhood \n",
    "nh_size = 100\n",
    "# Defines neighbor-generation function with the respective parameters\n",
    "nh_function = prm_rnd_int_ibound(prob=0.3, lb=0, ub=max_rep)\n",
    "# Defines HC's parameters\n",
    "pars[HillClimbing] = {\"initializer\": sp_init, \"nh_function\": nh_function, \"nh_size\": nh_size}"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "silver-tiffany",
   "metadata": {},
   "source": [
    "## 2.3. Simulated annealing (SA).\n",
    "The second branch, called simulated annealing (SA), extends HC by formulating a non-negative probability of replacing the best-so-far solution by its neighbor when the latter is worse. Traditionally, such a probability is small and decreases as the search advances. The strategy adopted by SA is especially useful when the search is prematurely tagnated at a locally sub-optimal point in $S$."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "confident-history",
   "metadata": {},
   "source": [
    "The cell in below adds ``SimulatedAnnealing`` to ``pars``. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 44,
   "id": "promotional-cornwall",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Defines SA's parameters\n",
    "pars[SimulatedAnnealing] = {\"initializer\": sp_init, \"nh_function\": nh_function, \"nh_size\": nh_size, \"control\": 1.0, \"update_rate\": 0.9}"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ordinary-ballet",
   "metadata": {},
   "source": [
    "## 2.4. Genetic algorithm (GA).\n",
    "Based on the number of candidate solutions they handle at each step, the metaheuristics can be categorized into single-point (SP) and population-based (PB) approaches. \n",
    "The search procedure in the SP metaheuristics is generally guided by the information provided by a single candidate solution from $S$, usually the best-so-far solution, that is gradually evolved in a well-defined manner in hope to find the global optimum. The abovementioned HC and SA are examples of SP metaheuristics as the search is performed by exploring the neighborhood $N(i)$, where $i$ is the current best solution. Contrarily, the search procedure in PB metaheuristics is generally guided by the information shared by a set of candidate solutions and the exploitation of its collective behavior in different ways. In abstract terms, one can say that every PB metaheuristics shares, at least, the following two features: an object representing the set of simultaneously exploited candidate solutions (i.e., the population), and a procedure to *move* them across $S$.\n",
    "\n",
    "Genetic Algorithm (GAs) is a meta-heuristic introduced by J. Holland which was strongly inspired by Darwin's theory of evolution by means of natural selection. Conceptually, the algorithm starts with a random-like population of candidate solutions (called *chromosomes*). Then, by mimicking the natural selection and genetically inspired variation operators, such as the crossover and the mutation, the algorithm breeds a population of the next-generation candidate solutions (called the *offspring population*), that replaces the previous population (a.k.a. the *parent population*). This procedure is iterated until reaching some stopping criteria, like a maximum\n",
    "number of iterations (also called *generations*)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "virtual-calculation",
   "metadata": {},
   "source": [
    "The cell in below adds ``GeneticAlgorithm`` to ``pars``. It uses a slightly different initializer, specially designed to efficiently initialize PB metaheuristics, called ``prm_rnd_mint``, that returns a matrix generated under discrete uniform distribution between with user-specified lower and upper bounds. Note that the mutation function is the neighbor-generation function, that was used for the aforementioned LS algorithms, and the population's size is equivalent to the neighborhood's size; this is done to foster the equivalency between LS and PB metaheuristics. Finally, the crossover is one-point, the elite is always a member of the population and parents' reproduction is enabled (``elitism=True`` and ``reproduction=True``)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 45,
   "id": "particular-blair",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Defines a population-based (PB) initializer\n",
    "pb_init = prm_rnd_mint(0, max_rep+1)\n",
    "# Defines GA's parameters\n",
    "pars[GeneticAlgorithm] = {\"pop_size\": nh_size, \"initializer\": pb_init, \"selector\": prm_tournament(pressure=0.05), \"mutator\": nh_function, \n",
    "                          \"crossover\": one_point_xo, \"p_m\": 0.3, \"p_c\": 0.7, \"elitism\": True, \"reproduction\": True}"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "finite-chapter",
   "metadata": {},
   "source": [
    "# 3. Executes the experiment."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "active-karaoke",
   "metadata": {},
   "source": [
    "Note that *many* parameters and functions are shared across different algorithms in the experiment. This allows to increase the control and comparability between different algorithmic approaches when solving a given problem's instance."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 46,
   "id": "emotional-alcohol",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "<class 'gpol.algorithms.random_search.RandomSearch'>\n",
      "\t initializer <function prm_rnd_vint.<locals>.rnd_vint at 0x000001324FFB9CA0>\n",
      "<class 'gpol.algorithms.local_search.HillClimbing'>\n",
      "\t initializer <function prm_rnd_vint.<locals>.rnd_vint at 0x000001324FFB9CA0>\n",
      "\t nh_function <function prm_rnd_int_ibound.<locals>.rnd_int_ibound at 0x000001324FF9CEE0>\n",
      "\t nh_size 100\n",
      "<class 'gpol.algorithms.local_search.SimulatedAnnealing'>\n",
      "\t initializer <function prm_rnd_vint.<locals>.rnd_vint at 0x000001324FFB9CA0>\n",
      "\t nh_function <function prm_rnd_int_ibound.<locals>.rnd_int_ibound at 0x000001324FF9CEE0>\n",
      "\t nh_size 100\n",
      "\t control 1.0\n",
      "\t update_rate 0.9\n",
      "<class 'gpol.algorithms.genetic_algorithm.GeneticAlgorithm'>\n",
      "\t pop_size 100\n",
      "\t initializer <function prm_rnd_mint.<locals>.rnd_mint at 0x000001324FFB93A0>\n",
      "\t selector <function prm_tournament.<locals>.tournament at 0x000001324FFB9820>\n",
      "\t mutator <function prm_rnd_int_ibound.<locals>.rnd_int_ibound at 0x000001324FF9CEE0>\n",
      "\t crossover <function one_point_xo at 0x0000013238F749D0>\n",
      "\t p_m 0.3\n",
      "\t p_c 0.7\n",
      "\t elitism True\n",
      "\t reproduction True\n"
     ]
    }
   ],
   "source": [
    "for isa_type, isa_pars in pars.items():\n",
    "    print(isa_type)\n",
    "    for p_name, p_val in isa_pars.items():\n",
    "        print(\"\\t\", p_name, p_val)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "outer-venice",
   "metadata": {},
   "source": [
    "Defines the computational resources for the experiment: the number of iterations."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 47,
   "id": "suitable-shoot",
   "metadata": {},
   "outputs": [],
   "source": [
    "n_iter = 30"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "established-burning",
   "metadata": {},
   "source": [
    "Loops the afore-defined ``pars`` dictionary containing algorithms' and the underlying parameters. Note that besides algorithm-specific parameters, the constructor of an instance of a search algorithm also receives the random state to initialize a pseudorandom number generator (called ``seed``), and the specification of the processing ``device`` (either CPU or GPU).\n",
    "\n",
    "The ``solve`` method has the same signature for all the search algorithms and, in this example, includes the following parameters: \n",
    "-  ``n_iter``: number of iterations to conduct the search;\n",
    "-  ``tol``: minimum required fitness improvement for ``n_iter_tol`` consecutive iterations to continue the search. When the fitness is not improving by at least ``tol`` for ``n_iter_tol`` consecutive iterations, the search will be automatically interrupted;\n",
    "-  ``n_iter_tol``: maximum number of iterations to not meet ``tol`` improvement;\n",
    "-  ``verbose``: verbosity's detail-level;\n",
    "-  ``log``: log-files' detail-level (if exists)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 48,
   "id": "cardiovascular-bosnia",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "-------------------------------------------------\n",
      "           |           Best solution            |\n",
      "-------------------------------------------------\n",
      "Generation   Length   Fitness              Timing\n",
      "0            33       294.668               0.007\n",
      "1            33       294.668               0.000\n",
      "2            33       294.668               0.000\n",
      "3            33       294.668               0.000\n",
      "4            33       294.668               0.000\n",
      "5            33       294.668               0.000\n",
      "Algorithm: RandomSearch\n",
      "Best solution's fitness: 294.668\n",
      "Best solution: tensor([2., 1., 1., 2., 1., 3., 1., 3., 1., 1., 4., 3., 4., 2., 2., 1., 1.])\n",
      "\n",
      "--------------------------------------------------------------------------------------\n",
      "           |           Best solution              |           Neighborhood           |\n",
      "--------------------------------------------------------------------------------------\n",
      "Generation | Length   Fitness              Timing | AVG Fitness           STD Fitness\n",
      "0          | 33       294.668               0.005 | -1                             -1\n",
      "1          | 37       351.685               0.007 | 64.9286                   126.945\n",
      "2          | 37       384.453               0.006 | 69.669                    136.153\n",
      "3          | 36       387.791               0.007 | 64.5588                   138.862\n",
      "4          | 38       422.883               0.006 | 130.08                    178.769\n",
      "5          | 38       422.883               0.007 | 86.3339                   163.757\n",
      "6          | 38       427.237               0.006 | 69.6723                   149.815\n",
      "7          | 38       427.237               0.006 | 83.6292                   163.392\n",
      "8          | 38       432.524               0.006 | 59.7217                   143.226\n",
      "9          | 39       446.316               0.007 | 55.7892                   139.399\n",
      "10         | 39       448.919               0.006 | 37.8585                   121.286\n",
      "11         | 39       448.919               0.009 | 90.8001                   172.204\n",
      "12         | 39       448.919               0.006 | 78.5288                   163.495\n",
      "13         | 39       448.919               0.006 | 81.3229                   168.897\n",
      "14         | 39       448.919               0.006 | 83.4492                   168.017\n",
      "Algorithm: HillClimbing\n",
      "Best solution's fitness: 448.919\n",
      "Best solution: tensor([3., 1., 2., 3., 3., 3., 3., 1., 2., 3., 2., 1., 4., 3., 3., 1., 1.])\n",
      "\n",
      "--------------------------------------------------------------------------------------\n",
      "           |           Best solution              |           Neighborhood           |\n",
      "--------------------------------------------------------------------------------------\n",
      "Generation | Length   Fitness              Timing | AVG Fitness           STD Fitness\n",
      "0          | 33       294.668               0.005 | -1                             -1\n",
      "1          | 37       351.685               0.006 | 64.9286                   126.945\n",
      "2          | 37       384.453               0.006 | 69.669                    136.153\n",
      "3          | 36       387.791               0.006 | 64.5588                   138.862\n",
      "4          | 38       422.883               0.006 | 130.08                    178.769\n",
      "5          | 38       422.883               0.008 | 86.3339                   163.757\n",
      "6          | 38       427.237               0.006 | 69.6723                   149.815\n",
      "7          | 38       427.237               0.006 | 83.6292                   163.392\n",
      "8          | 38       432.524               0.006 | 59.7217                   143.226\n",
      "9          | 39       446.316               0.006 | 55.7892                   139.399\n",
      "10         | 39       448.919               0.007 | 37.8585                   121.286\n",
      "11         | 39       448.919               0.006 | 90.8001                   172.204\n",
      "12         | 39       448.919               0.010 | 78.5288                   163.495\n",
      "13         | 39       448.919               0.006 | 81.3229                   168.897\n",
      "14         | 39       448.919               0.006 | 83.4492                   168.017\n",
      "Algorithm: SimulatedAnnealing\n",
      "Best solution's fitness: 448.919\n",
      "Best solution: tensor([3., 1., 2., 3., 3., 3., 3., 1., 2., 3., 2., 1., 4., 3., 3., 1., 1.])\n",
      "\n",
      "--------------------------------------------------------------------------------------\n",
      "           |           Best solution              |            Population            |\n",
      "--------------------------------------------------------------------------------------\n",
      "Generation | Length   Fitness              Timing | AVG Fitness           STD Fitness\n",
      "0          | 33       294.668               0.000 | 2.94668                   29.4668\n",
      "1          | 33       294.668               0.009 | 0                               0\n",
      "2          | 35       306.191               0.008 | 9.03928                   51.6654\n",
      "3          | 33       315.448               0.008 | 15.2244                   66.7131\n",
      "4          | 35       347.648               0.007 | 40.1186                   104.457\n",
      "5          | 35       360.235               0.010 | 102.486                   147.231\n",
      "6          | 35       360.235               0.007 | 207.307                   150.485\n",
      "7          | 36       361.44                0.008 | 234.753                   148.028\n",
      "8          | 35       372.447               0.007 | 255.138                   148.957\n",
      "9          | 36       386.239               0.008 | 281.203                   146.077\n",
      "10         | 37       394.01                0.008 | 262.879                   165.082\n",
      "11         | 37       394.02                0.009 | 238.16                    179.862\n",
      "12         | 37       400.914               0.009 | 257.37                    178.072\n",
      "13         | 37       406.923               0.008 | 238.759                   188.366\n",
      "14         | 37       406.923               0.007 | 310.437                   161.152\n",
      "Algorithm: GeneticAlgorithm\n",
      "Best solution's fitness: 406.923\n",
      "Best solution: tensor([4., 1., 1., 3., 3., 3., 1., 2., 1., 2., 1., 4., 4., 2., 3., 1., 1.])\n",
      "\n"
     ]
    }
   ],
   "source": [
    "for isa_type, isa_pars in pars.items():\n",
    "    isa = isa_type(pi=pi, **isa_pars, seed=seed, device=device)\n",
    "    # n_iter*pop_size if isinstance(isa, RandomSearch) else n_iter  # equivalency for the RS\n",
    "    isa.solve(n_iter=n_iter, tol=10, n_iter_tol=5, verbose=2, log=0)\n",
    "    print(\"Algorithm: {}\".format(isa_type.__name__))\n",
    "    print(\"Best solution's fitness: {:.3f}\".format(isa.best_sol.fit))\n",
    "    print(\"Best solution:\", isa.best_sol.repr_, end=\"\\n\\n\")"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
