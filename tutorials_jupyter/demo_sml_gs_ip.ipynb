{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "loaded-richmond",
   "metadata": {},
   "source": [
    "# Solving supervised machine learning problems (from the perspective of inductive programming).\n",
    "Inductive program synthesis (aka inductive programming) is a subfield in the program synthesis that studies program generation from incomplete information, namely from the examples for the desired input/output behavior of the program. Genetic programming (GP) is one of the numerous approaches for the inductive synthesis characterized by performing the search in the space of syntactically correct programs of a given programming language.\n",
    "\n",
    "In the context of supervised machine learning (SML) problem-solving, one can define the task of a GP algorithm as the program/function induction from input/output examples that identifies the mapping $f:S\\mapsto R$ in the best possible way, generally measured through solution’s generalization ability on previously unseen data.\n",
    "\n",
    "Geometric Semantic Genetic Programming (GSGP) is a variant of GP where the standard crossover and mutation operators are replaced by the so-called Geometric Semantic Operators (GSOs).\n",
    "\n",
    "## SMLGS problem type.\n",
    "Given the definitions provided above and in order to make it possible to perform automatic induction of programs from the input/output-examples, we have conceptualized a module called ``inductive_programming`` which contains different problem types, materialized as classes. One of them, called ``SMLGS``, a subclass of ``Problem``, aims at supporting the SML problem-solving, more specifically the symbolic regression and binary classification, by means of GSGP.\n",
    "\n",
    "\n",
    "<img src=\"https://media.springernature.com/original/springer-static/image/chp%3A10.1007%2F978-3-319-44003-3_1/MediaObjects/393665_1_En_1_Fig6_HTML.gif\" alt=\"Drawing\" style=\"width: 300px;\"/>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "smart-stable",
   "metadata": {},
   "source": [
    "# 1. Create an instance of ``SMLGS``"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "orange-distinction",
   "metadata": {},
   "source": [
    "Loads the necessary classes and functions."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "apart-melissa",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Imports utility libraries \n",
    "import os\n",
    "import datetime\n",
    "import pandas as pd\n",
    "# Imports PyTorch\n",
    "import torch\n",
    "# Imports problems\n",
    "from gpol.problems.inductive_programming import SMLGS\n",
    "from gpol.utils.datasets import load_boston\n",
    "from gpol.utils.utils import train_test_split, rmse\n",
    "from gpol.utils.inductive_programming import function_map, prm_reconstruct_tree, _execute_tree, _get_tree_depth\n",
    "# Imports metaheuristics \n",
    "from gpol.algorithms.genetic_algorithm import GSGP\n",
    "# Imports operators\n",
    "from gpol.operators.initializers import rhh, prm_full\n",
    "from gpol.operators.selectors import prm_tournament\n",
    "from gpol.operators.variators import prm_efficient_gs_xo, prm_efficient_gs_mtn"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bored-jesus",
   "metadata": {},
   "source": [
    "Creates an instance of ``SMLGS`` problem. The search space (*S*) of an instance of ``SMLGS`` problem consists of the following key-value pairs:\n",
    "- ``\"n_dims\"`` is the number of input features (aka input dimensions) in the underlying ``SMLGS`` problem's instance;\n",
    "- ``\"function set\"`` is the set of primitive functions;\n",
    "- ``\"constant set\"`` is the set of constants to draw terminals from;\n",
    "- ``\"p_constants\"`` is the probability of generating a constant when sampling a terminal; and\n",
    "-  ``max_init_depth`` is the trees’ maximum depth during the initialization.\n",
    "\n",
    "Besides the traditional triplet ``sspace``, ``ffunction`` and ``min_``, one has to provide the problem's instance with the input data (``X`` and ``y`` tensors), partitions' indexes (``train_indices`` and ``test_indices``), and the size of the batches (``batch_size``)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "happy-metabolism",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Defines the processing device and random state 's seed\n",
    "device, seed  = 'cpu', 0  # 'cuda' if torch.cuda.is_available() else 'cpu', 0\n",
    "# Loads and allocates the data on the processing device\n",
    "X, y = load_boston(X_y=True)\n",
    "X = X.to(device)\n",
    "y = y.to(device)\n",
    "# Defines parameters for the data usage\n",
    "batch_size, shuffle, p_test = 50, True, 0.3\n",
    "# Performs train/test split\n",
    "train_indices, test_indices = train_test_split(X=X, y=y, p_test=p_test, shuffle=shuffle, indices_only=True, seed=seed)\n",
    "# Characterizes the program elements: function and constant sets\n",
    "f_set = [function_map[\"add\"], function_map[\"sub\"], function_map[\"mul\"], function_map[\"div\"]]\n",
    "c_set = torch.tensor([-1.0, -0.5, 0.5, 1.0], device=device)\n",
    "# Creates the search space\n",
    "sspace = {\"n_dims\": X.shape[1], \"function_set\": f_set, \"p_constants\": 0.1, \"constant_set\": c_set, \"max_init_depth\": 5}\n",
    "# Creates problem's instance\n",
    "pi = SMLGS(sspace=sspace, ffunction=rmse, X=X, y=y, train_indices=train_indices, test_indices=test_indices,\n",
    "           batch_size=100, min_=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "formal-advertiser",
   "metadata": {},
   "source": [
    "# 2. Parametrize the GSGP algorithm."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "charged-sampling",
   "metadata": {},
   "source": [
    "The cell in below creates a dictionary, called ``pars``, to store GSGP-specific parameters. The mutation and crossover operators, ``prm_efficient_gs_mtn`` and ``prm_efficient_gs_xo`` respectively, were specifically designed to resemble the implementation proposed in *A C++ framework for geometric semantic genetic programming* by Castelli et al.\n",
    "\n",
    "The tensor ``ms`` represents the steps of the GSM operator; if it is a single-valued tensor, then the mutation step is always the same and equals ``ms``; if it is a vector, then, at each call of the operator, the mutation step is selected at random from ``ms``. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "activated-exhaust",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Defines population's size\n",
    "pop_size = 100\n",
    "# Creates single trees' initializer for the GSOs\n",
    "sp_init = prm_full(sspace)  \n",
    "# Generates GSM's steps\n",
    "to, by = 5.0, 0.25  \n",
    "ms = torch.arange(by, to + by, by, device=device)\n",
    "# Defines selection's pressure and mutation's probability\n",
    "pars = {\"pop_size\": pop_size, \"initializer\": rhh, \"selector\": prm_tournament(pressure=0.1),\n",
    "        \"mutator\": prm_efficient_gs_mtn(X, sp_init, ms), \"crossover\": prm_efficient_gs_xo(X, sp_init),\n",
    "        \"p_m\": 0.3, \"p_c\": 0.7, \"elitism\": True, \"reproduction\": False}"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "spread-reviewer",
   "metadata": {},
   "source": [
    "# 3. Prepares the connection strings.\n",
    "Following the implementation proposed in *A C++ framework for geometric semantic genetic programming* by Castelli et al., the initial population of trees and the intermediary random trees, generated throughout the evolution during GSOs' application, must be stored on disk. The cell in below creates the necessary paths: \n",
    "-  ``path`` is a connection string towards GSGP's main log-folder;\n",
    "-  ``path_init_pop`` is a connection string towards initial population's repository;\n",
    "-  ``path_rts`` is a connection string towards random trees' repository; and\n",
    "-  ``path_hist`` is a connection string towards the history's file (a file that stores solutions' genealogy)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "horizontal-patch",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Creates the experiment's label\n",
    "experiment_label = \"SMLGS\"  # SML approached from the perspective of Inductive Programming using GSGP\n",
    "time_id = str(datetime.datetime.now().date()) + \"_\" + str(datetime.datetime.now().hour) + \"_\" + \\\n",
    "          str(datetime.datetime.now().minute) + \"_\" + str(datetime.datetime.now().second)\n",
    "# Creates general path\n",
    "path = os.path.join(os.getcwd(), experiment_label + \"_\" + time_id)\n",
    "# Defines a connection string to store random trees\n",
    "path_rts = os.path.join(path, \"reconstruct\", \"rts\")\n",
    "if not os.path.exists(path_rts):\n",
    "    os.makedirs(path_rts)\n",
    "# Defines a connection string to store the initial population\n",
    "path_init_pop = os.path.join(path, \"reconstruct\", \"init_pop\")\n",
    "if not os.path.exists(path_init_pop):\n",
    "    os.makedirs(path_init_pop)\n",
    "# Creates a connection string towards the history's file\n",
    "path_hist = os.path.join(path, \"reconstruct\", experiment_label + \"_seed_\" + str(seed) + \"_history.csv\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "alternate-defense",
   "metadata": {},
   "source": [
    "# 4. Executes the experiment."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "limiting-success",
   "metadata": {},
   "source": [
    "Defines the computational resources for the experiment: the number of iterations."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "polished-gentleman",
   "metadata": {},
   "outputs": [],
   "source": [
    "n_iter = 30"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "broad-extension",
   "metadata": {},
   "source": [
    "The code in below creates an instance of type ``GSGP`` with the aforementioned parameters. At the end of the search, the method ``write_history`` stores on disk the solutions' genealogy.\n",
    "\n",
    "Note that besides algorithm-specific parameters, the constructor of an instance of ``GSGP`` algorithm also receives:\n",
    "-  ``path_init_pop`` is path where the initial trees will be stored;\n",
    "-  ``path_rts`` is path where the random trees generated throughout the evolution will be stored;\n",
    "-  ``seed`` is used to initialize a pseudorandom number generator; and\n",
    "-  ``device`` is the specification of the processing (either CPU or GPU).\n",
    "\n",
    "The ``solve`` method has the same signature for all the search algorithms and, in this case, includes the following parameters: \n",
    "-  ``n_iter``: number of iterations to conduct the search;\n",
    "-  ``tol``: minimum required (training) fitness improvement for ``n_iter_tol`` consecutive iterations to continue the search. When the fitness is not improving by at least ``tol`` for ``n_iter_tol`` consecutive iterations, the search will be automatically interrupted;\n",
    "-  ``n_iter_tol``: maximum number of iterations to not meet ``tol`` improvement;\n",
    "-  ``test_elite``: a flag indicating whether the best-so-far solution (𝑖) should be evaluated on test partition (regards SML-OPs);\n",
    "-  ``verbose``: verbosity's detail-level;\n",
    "-  ``log``: log-files' detail-level (if exists)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "id": "weekly-occasion",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "-------------------------------------------------------------------------------------------------------\n",
      "           |                    Best solution                      |            Population            |\n",
      "-------------------------------------------------------------------------------------------------------\n",
      "Generation | Length   Fitness          Test Fitness         Timing | AVG Fitness           STD Fitness\n",
      "-------------------------------------------------------------------------------------------------------\n",
      "0          | 506      11.2839          12.8798               0.055 | 5.2491e+08            5.24537e+09\n",
      "1          | 506      11.2839          13.2341               0.096 | 19.0067                   2.95073\n",
      "2          | 506      10.121           10.4062               0.096 | 14.2705                   1.43619\n",
      "3          | 506      10.121           10.7162               0.090 | 13.3954                     1.231\n",
      "4          | 506      9.17288          8.26451               0.099 | 10.7042                  0.733182\n",
      "5          | 506      7.62583          7.53925               0.085 | 9.47217                  0.611304\n",
      "6          | 506      7.54392          7.36802               0.098 | 9.01072                  0.819879\n",
      "7          | 506      7.54392          7.6432                0.087 | 8.87262                   0.40885\n",
      "8          | 506      6.79107          7.95744               0.096 | 7.34892                  0.416955\n",
      "9          | 506      6.79107          7.7124                0.100 | 8.46088                  0.532295\n",
      "10         | 506      6.60293          6.8049                0.092 | 7.1521                   0.429876\n",
      "11         | 506      6.60293          7.79044               0.098 | 7.20992                  0.581137\n",
      "12         | 506      6.10853          6.67109               0.102 | 6.52282                  0.434309\n",
      "13         | 506      6.10853          6.55068               0.099 | 7.77053                  0.491859\n",
      "14         | 506      6.10853          7.63303               0.106 | 6.60325                   0.48083\n",
      "15         | 506      6.10853          6.75855               0.090 | 6.42357                  0.378534\n",
      "16         | 506      5.87449          5.89995               0.100 | 6.18514                   0.36733\n",
      "17         | 506      5.73621          6.55869               0.090 | 6.19314                  0.370132\n",
      "18         | 506      5.73621          6.63215               0.097 | 6.73077                  0.439363\n",
      "19         | 506      5.73621          6.61377               0.096 | 7.55409                  0.470595\n",
      "20         | 506      5.73621          6.19271               0.091 | 6.53171                  0.360147\n",
      "21         | 506      5.48863          6.35377               0.094 | 5.92726                  0.522633\n",
      "22         | 506      5.15343          6.72808               0.099 | 5.43865                   0.41742\n",
      "23         | 506      5.15343          5.96808               0.097 | 5.71924                  0.530552\n",
      "24         | 506      5.15343          6.15334               0.099 | 5.54216                  0.511395\n",
      "25         | 506      5.15343          6.36306               0.100 | 6.43797                  0.418912\n",
      "26         | 506      4.90332          5.64104               0.102 | 5.49067                  0.523734\n",
      "27         | 506      4.90332          5.50202               0.104 | 7.64401                  0.451151\n",
      "28         | 506      4.90332          6.53446               0.098 | 6.02377                  0.414011\n",
      "29         | 506      4.34947          5.48667               0.110 | 4.9272                   0.632087\n",
      "30         | 506      4.34947          5.96227               0.106 | 5.2334                   0.478879\n",
      "Algorithm: GSGP\n",
      "Best solution's fitness: 4.349\n",
      "Best solution's test fitness: 5.962\n"
     ]
    }
   ],
   "source": [
    "isa = GSGP(pi=pi, path_init_pop=path_init_pop, path_rts=path_rts, seed=seed, device=device, **pars)\n",
    "isa.solve(n_iter=n_iter, tol=0.1, n_iter_tol=5, test_elite=True, verbose=2, log=0)\n",
    "isa.write_history(path_hist)\n",
    "print(\"Algorithm: {}\".format(isa.__name__))\n",
    "print(\"Best solution's fitness: {:.3f}\".format(isa.best_sol.fit))\n",
    "print(\"Best solution's test fitness: {:.3f}\".format(isa.best_sol.test_fit))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "perceived-douglas",
   "metadata": {},
   "source": [
    "# 5. Reconstructs the tree.\n",
    "In order to reconstruct the tree, the user needs to:\n",
    "1.  read the historical records (the solutions' genealogy);\n",
    "2.  create a parametrized reconstruction function using ``prm_reconstruct_tree``. The latter receives the historical records' ``pandas.DataFrame``, the paths towards the initial and random trees, and the processing devices that was used to conduct the search.\n",
    "3.  choose the solution to reconstruct (by specifying its index);\n",
    "4.  call the reconstruction function by passing the index of the desired solution."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "vital-clause",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Starting index (chosen individual): 0_29_o2_xo_4760\n",
      "Individual's info:\n",
      " Iter                      29\n",
      "Operator           crossover\n",
      "T1           0_28_o1_xo_4540\n",
      "T2          0_28_o1_mtn_4566\n",
      "Tr           0_29_rt_xo_4758\n",
      "ms                        -1\n",
      "Fitness              4.34947\n",
      "Name: 0_29_o2_xo_4760, dtype: object\n"
     ]
    }
   ],
   "source": [
    "history = pd.read_csv(os.path.join(path_hist), index_col=0)\n",
    "# Creates a reconstruction function\n",
    "reconstructor = prm_reconstruct_tree(history, path_init_pop, path_rts, device)\n",
    "# Chooses the most fit individual to reconstruct\n",
    "start_idx = history[\"Fitness\"].idxmin()\n",
    "print(\"Starting index (chosen individual):\", start_idx)\n",
    "print(\"Individual's info:\\n\", history.loc[start_idx])\n",
    "# Reconstructs the individual\n",
    "ind = reconstructor(start_idx)\n",
    "print(\"Automatically reconstructed individual's representation:\\n\", ind[0:30])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "informational-heather",
   "metadata": {},
   "source": [
    "Prints individuals length and depth."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "geographic-joshua",
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"Length\", len(ind))\n",
    "print(\"Depth\", _get_tree_depth(ind))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "improved-moore",
   "metadata": {},
   "source": [
    "Executes the reconstructed individual on the whole dataset. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "adjusted-engine",
   "metadata": {},
   "outputs": [],
   "source": [
    "y_pred = _execute_tree(ind, X)\n",
    "print(\"Individual's RMSE on X: \", rmse(y, y_pred))"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
