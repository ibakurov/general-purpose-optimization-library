def main():
    import torch
    from torch.utils.data import TensorDataset, DataLoader

    from gpol.problems.inductive_programming import SML
    from gpol.utils.datasets import load_boston
    from gpol.algorithms.local_search import HillClimbing
    from gpol.algorithms.genetic_algorithm import GeneticAlgorithm
    from gpol.operators.initializers import rhh, prm_grow, grow
    from gpol.operators.selectors import prm_double_tournament
    from gpol.operators.variators import prm_subtree_mtn, swap_xo
    from gpol.utils.utils import train_test_split, rmse
    from gpol.utils.inductive_programming import function_map, _prune_tree_dict, simplify_

    # ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    # > > > Demonstration of how to create an instance of SML-IPOP
    # Defines the processing device and random state 's seed
    device, seed, p_test = 'cpu', 0, 0.3
    # Imports the data
    X, y = load_boston(X_y=True)
    # Performs train/test split
    X_train, X_test, y_train, y_test = train_test_split(X, y, p_test=p_test, seed=seed)
    # Creates two objects of type TensorDataset: one for training, another for test data
    ds_train = TensorDataset(X_train, y_train)
    ds_test = TensorDataset(X_test, y_test)
    # Creates training and test DataLoader
    batch_size, shuffle, num_workers = len(X_train), True, 0
    dl_train = DataLoader(dataset=ds_train, batch_size=batch_size, shuffle=shuffle, num_workers=num_workers)
    dl_test = DataLoader(dataset=ds_test, batch_size=batch_size, shuffle=shuffle, num_workers=num_workers)
    # Characterizes the program elements: function set
    function_set = [function_map["add"], function_map["sub"], function_map["mul"], function_map["div"]]
    # Characterizes the program elements: constant set
    constant_set = torch.tensor([-1.0, -0.5, 0.5, 1.0], device=device)
    # Defines the search space
    sspace = {"n_dims": X.shape[1], "function_set": function_set, "constant_set": constant_set, "p_constants": 0.1,
              "max_init_depth": 5, "max_depth": 15, "n_batches": 1}
    # Creates problem's instance
    pi = SML(sspace, rmse, dl_train, dl_test, min_=True)

    # ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    # > > >´Defines a highly redundant tree manually to show-case pruning algorithms
    repr_tree = [function_map["sub"], function_map["add"], function_map["add"], 1, 1, function_map["add"], 1,
                 function_map["mul"], torch.tensor(1.0), function_map["mul"], function_map["add"], torch.tensor(1.0),
                 function_map["add"], torch.tensor(1.0), function_map["add"], torch.tensor(1.0), function_map["mul"],
                 torch.tensor(1.0), torch.tensor(3.0), function_map["add"], torch.tensor(2.0), 1, function_map["mul"],
                 function_map["div"], 0, 0, function_map["mul"], function_map["mul"], torch.tensor(6.0),
                 torch.tensor(0.5), function_map["mul"], torch.tensor(-0.6), function_map["div"], torch.tensor(6.0), 3]
    print("Original (highly redundant) input tree:\n", repr_tree, end="\n\n")

    # Perform numeric (exact) simplification
    repr_new_num, intermediate_result, intermediate_result_new = _prune_tree_dict(repr_tree, X_train, re_run=True)
    print("Tree after (exact) numeric simplification:\n", repr_new_num)
    print("RMSE of original tree:\n", rmse(y_train, intermediate_result))
    print("RMSE of simplified tree:\n", rmse(y_train, intermediate_result), end="\n\n")

    # Perform algebraic simplification
    operators_map = {function_map["sub"]: "-", function_map["add"]: "+",
                     function_map["mul"]: "*", function_map["div"]: "/"}
    repr_new_alg = simplify_(repr_tree, sspace["n_dims"], operators_map)
    print("Tree after algebraic simplification:\n", repr_new_alg)

    # ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    # > > >´Defines algorithms' parameters
    pop_size, n_iter = 100, 50
    # Defines HC's parameters
    single_initializer = prm_grow(sspace)
    pars = {}
    pars = {HillClimbing: {"initializer": grow, "nh_function": prm_subtree_mtn(single_initializer), "nh_size": pop_size}}
    # Defines GP's parameters
    p_m = 0.3
    pars[GeneticAlgorithm] = {"initializer": rhh, "selector": prm_double_tournament(0.05, "fitness", "size"),
                              "mutator": prm_subtree_mtn(single_initializer), "crossover": swap_xo, "p_m": p_m,
                              "p_c": 1.0-p_m, "pop_size": pop_size, "elitism": True, "reproduction": True}

    # ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    # > > >´Shows how to use the algorithms to solve the PI
    for isa_type, isa_pars in pars.items():
        # Creates an instance of a solve algorithm
        isa = isa_type(pi=pi, **isa_pars, seed=seed, device=device)
        # Solves the PI
        isa.solve(n_iter=n_iter, test_elite=True, verbose=2, log=0)
        print("Algorithm: {}".format(isa_type.__name__))
        print("Best solution's fitness: {:.3f}".format(isa.best_sol.fit))
        print("Best solution:", isa.best_sol.repr_)
        # Perform numeric simplification
        repr_new_num, _, _ = _prune_tree_dict(isa.best_sol.repr_, X_train)
        print("Tree after (exact) numeric simplification:\n", repr_new_num)
        # Performs algebraic simplification
        repr_new_alg = simplify_(isa.best_sol.repr_, sspace["n_dims"], operators_map)
        print("Tree after algebraic simplification:\n", repr_new_alg, end="\n\n")


if __name__ == "__main__":
    main()
